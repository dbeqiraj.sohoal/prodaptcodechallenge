rootProject.name = "ProdaptCodeChallenge"
include(":app")
include(":domain")
include(":data")
includeBuild("Internal")
include(":datastore")
include(":annotations")
include(":annotations-processor")
include(":ProdaptKit")
