import Build_gradle.and
import Build_gradle.dep
import org.jetbrains.kotlin.kapt3.base.Kapt.kapt

typealias and = com.dbeqiraj.internal.Android
typealias dep = com.dbeqiraj.internal.Dependencies

plugins {
    id("com.android.library")
    id("kotlin-android")
    id("internal")
    kotlin("kapt")
}

android {

    compileSdk = and.compileSdk

    defaultConfig {

        minSdk = and.minSdk
        targetSdk = and.targetSdk

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }
    buildTypes {
        release {
            isMinifyEnabled = false
            proguardFiles(getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro")
        }
    }

}

dependencies {

    dep.compose.apply { // https://developer.android.com/jetpack/compose
        implementation(pagingCompose)
    }

    dep.room.apply { // https://developer.android.com/jetpack/androidx/releases/room
        implementation(runtime)
        implementation(ktx)
        kapt(compiler)
    }

    dep.other.apply { // Miscellaneous required libraries
        implementation(kotlinxCoroutines)
        implementation(serialization)
    }

    dep.libraries.apply {
        implementation(project(prodaptKit))
        implementation(project(mapOf("path" to annotations)))
        kapt(project(mapOf("path" to annotationsProcessor)))
    }
}
