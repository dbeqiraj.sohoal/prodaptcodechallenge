/*
 *     Created by Dionis Beqiraj on 10/08/2022, 16:42
 *     dbeqiraj.sohoal@gmail.com
 *     Last modified 10/08/2022, 16:41
 *     Copyright (c) 2022.
 *     All rights reserved.
 */

package com.dbeqiraj.domain.models.comments

import androidx.compose.runtime.Immutable
import kotlinx.serialization.SerialName

@Immutable
data class Comment(
    @SerialName("id")
    val id: Int,
    @SerialName("name")
    val name: String,
    @SerialName("email")
    val email: String,
    @SerialName("body")
    val body: String
) {

    companion object {
        fun mock() = Comment(
            id = 1,
            name = "Dionis Beqiraj",
            email = "dbeqiraj.sohoal@gmail.com",
            body = "This is an example of a comment!"
        )
    }
}
