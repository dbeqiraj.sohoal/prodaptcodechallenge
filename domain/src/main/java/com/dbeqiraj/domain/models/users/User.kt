/*
 *     Created by Dionis Beqiraj on 10/08/2022, 16:42
 *     dbeqiraj.sohoal@gmail.com
 *     Last modified 10/08/2022, 16:41
 *     Copyright (c) 2022.
 *     All rights reserved.
 */

package com.dbeqiraj.domain.models.users

import androidx.compose.runtime.Immutable
import androidx.room.ColumnInfo
import androidx.room.Embedded
import kotlinx.serialization.SerialName

@Immutable
data class User(
    @ColumnInfo(name = "user_id")
    @SerialName("id")
    val id: Int,
    @SerialName("name")
    val name: String,
    @SerialName("username")
    val username: String,
    @SerialName("email")
    val email: String,
    @SerialName("website")
    val website: String,
    @SerialName("phone")
    val phone: String,
    @Embedded
    @SerialName("address")
    val address: Address
) {
    @Immutable
    data class Address(
        @Embedded
        @SerialName("geo")
        val geo: Geo,
        @SerialName("city")
        val city: String
    ) {
        @Immutable
        data class Geo(
            @SerialName("lat")
            val lat: String,
            @SerialName("lng")
            val lng: String
        )
    }
}
