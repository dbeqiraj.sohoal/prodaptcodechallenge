/*
 *     Created by Dionis Beqiraj on 10/08/2022, 16:42
 *     dbeqiraj.sohoal@gmail.com
 *     Last modified 10/08/2022, 16:41
 *     Copyright (c) 2022.
 *     All rights reserved.
 */

package com.dbeqiraj.domain.models.posts

import androidx.compose.runtime.Immutable
import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.dbeqiraj.annotiations.Paginate
import com.dbeqiraj.domain.models.comments.Comment
import com.dbeqiraj.domain.models.users.User
import com.dbeqiraj.prodaptkit.SHA256
import com.dbeqiraj.prodaptkit.hashString
import com.dbeqiraj.prodaptkit.utils.Constants.PREFERRED_IMAGE_HEIGHT
import com.dbeqiraj.prodaptkit.utils.Constants.PREFERRED_IMAGE_WIDTH
import kotlinx.serialization.SerialName

@Entity
@Paginate
@Immutable
data class Post(
    @PrimaryKey
    @SerialName("id")
    val id: Int,
    @SerialName("userId")
    val userId: Int,
    @SerialName("title")
    val title: String,
    @SerialName("body")
    val body: String,
    @Embedded
    @SerialName("user")
    val user: User?,
    @SerialName("comments")
    val comments: List<Comment>?
) {
    val imageUrl: String
        get() {
            val seed = hashString(title, SHA256)
            return "$IMAGE_URL/$seed/$PREFERRED_IMAGE_WIDTH/$PREFERRED_IMAGE_HEIGHT"
        }

    companion object {
        private const val IMAGE_URL = "https://picsum.photos/seed/"

        // Mock
        fun mock() = Post(
            id = 1,
            userId = 1,
            title = "Post title",
            body = "Post body",
            user = User(
                id = 1,
                name = "Dionis Beqiraj",
                username = "dbeqiraj",
                email = "dbeqiraj.sohoal@gmail.com",
                website = "google.com",
                phone = "1-770-736-8031",
                address = User.Address(
                    city = "Gwenborough",
                    geo = User.Address.Geo(
                        lat = "-37.3159",
                        lng = "81.1496"
                    )
                )
            ),
            comments = listOf(Comment.mock())
        )
    }
}
