/*
 *     Created by Dionis Beqiraj on 10/08/2022, 16:42
 *     dbeqiraj.sohoal@gmail.com
 *     Last modified 10/08/2022, 16:41
 *     Copyright (c) 2022.
 *     All rights reserved.
 */

package com.dbeqiraj.domain.repositories

import com.dbeqiraj.domain.models.users.User

interface UsersRepository {

    suspend fun getUserById(id: Int): Result<User>
}
