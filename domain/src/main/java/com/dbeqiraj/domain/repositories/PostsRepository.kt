/*
 *     Created by Dionis Beqiraj on 10/08/2022, 16:42
 *     dbeqiraj.sohoal@gmail.com
 *     Last modified 10/08/2022, 16:41
 *     Copyright (c) 2022.
 *     All rights reserved.
 */

package com.dbeqiraj.domain.repositories

import androidx.paging.PagingData
import com.dbeqiraj.domain.models.posts.Post
import kotlinx.coroutines.flow.Flow

interface PostsRepository {

    fun getPosts(): Flow<PagingData<Post>>

    fun getPostById(id: Int): Flow<Post>
}
