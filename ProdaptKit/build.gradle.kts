typealias dep = com.dbeqiraj.internal.Dependencies
typealias and = com.dbeqiraj.internal.Android

plugins {
    id("com.android.library")
    id("kotlin-android")
    id("internal")

    kotlin("kapt")
}

android {

    compileSdk = and.compileSdk

    defaultConfig {

        minSdk = and.minSdk
        targetSdk = and.targetSdk

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }
    buildTypes {
        release {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }

}

dependencies {

    dep.hilt.apply { // https://dagger.dev/hilt/
        implementation(hiltAndroid)
        implementation(hiltWork)
        kapt(hiltCompiler)
        kapt(daggerHiltCompiler)
    }

    dep.playServices.apply { // https://developer.android.com/jetpack/compose
        implementation(playServicesLocation)
    }

    dep.other.apply { // Miscellaneous required libraries
        implementation(kotlinxCoroutines)
        implementation(gson)
        api(sunMail)
        api(timber)
    }

}