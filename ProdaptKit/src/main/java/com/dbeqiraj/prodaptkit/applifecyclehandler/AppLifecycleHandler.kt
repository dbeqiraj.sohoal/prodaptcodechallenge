package com.dbeqiraj.prodaptkit.applifecyclehandler

import android.app.Activity
import android.app.Application
import android.content.ComponentCallbacks2
import android.content.res.Configuration
import android.os.Bundle

class AppLifecycleHandler(private val lifecycleDelegate: LifecycleDelegate) :
    Application.ActivityLifecycleCallbacks, ComponentCallbacks2 {

    private var appInForeground = false

    override fun onActivityCreated(activity: Activity, savedInstanceState: Bundle?) {
        // Not implemented
    }

    override fun onActivityStarted(activity: Activity) {
        // Not implemented
    }

    override fun onActivityResumed(activity: Activity) {
        if (!appInForeground) {
            // App came in foreground detected
            appInForeground = true
            lifecycleDelegate.onAppForegrounded()
        }
    }

    override fun onActivityPaused(activity: Activity) {
        // Not implemented
    }

    override fun onActivityStopped(activity: Activity) {
        // Not implemented
    }

    override fun onActivitySaveInstanceState(activity: Activity, outState: Bundle) {
        // Not implemented
    }

    override fun onActivityDestroyed(activity: Activity) {
        // Not implemented
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        // Not implemented
    }

    override fun onLowMemory() {
        // Not implemented
    }

    override fun onTrimMemory(level: Int) {
        if (level == ComponentCallbacks2.TRIM_MEMORY_UI_HIDDEN) {
            appInForeground = false
            // App sent in background detected
            lifecycleDelegate.onAppBackgrounded()
        }
    }
}
