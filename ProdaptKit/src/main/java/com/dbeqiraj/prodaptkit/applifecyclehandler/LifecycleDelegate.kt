package com.dbeqiraj.prodaptkit.applifecyclehandler

interface LifecycleDelegate {
    fun onAppBackgrounded()
    fun onAppForegrounded()
}
