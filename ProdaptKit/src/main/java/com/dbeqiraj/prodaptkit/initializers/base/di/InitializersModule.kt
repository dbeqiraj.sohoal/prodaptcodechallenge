package com.dbeqiraj.prodaptkit.initializers.base.di

import com.dbeqiraj.prodaptkit.initializers.TimberInitializer
import com.dbeqiraj.prodaptkit.initializers.WorkerInitializer
import com.dbeqiraj.prodaptkit.initializers.base.AppInitializer
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import dagger.multibindings.IntoSet

@Module
@InstallIn(SingletonComponent::class)
abstract class InitializersModule {

    @Binds
    @IntoSet
    abstract fun provideTimberInitializer(bind: TimberInitializer): AppInitializer

    @Binds
    @IntoSet
    abstract fun provideWorkerInitializer(bind: WorkerInitializer): AppInitializer
}
