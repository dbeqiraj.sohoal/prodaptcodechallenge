package com.dbeqiraj.prodaptkit.initializers

import android.app.Application
import androidx.hilt.work.HiltWorkerFactory
import androidx.work.Configuration
import androidx.work.WorkManager
import com.dbeqiraj.prodaptkit.initializers.base.AppInitializer
import javax.inject.Inject

class WorkerInitializer @Inject constructor(private val workerFactory: HiltWorkerFactory) :
    AppInitializer, Configuration.Provider {

    override fun init(application: Application) {
        WorkManager.initialize(
            application,
            workManagerConfiguration
        )
    }

    override fun getWorkManagerConfiguration() = Configuration.Builder()
        .setMinimumLoggingLevel(android.util.Log.DEBUG)
        .setWorkerFactory(workerFactory)
        .build()
}
