package com.dbeqiraj.prodaptkit.initializers.base

import android.app.Application

interface AppInitializer {
    fun init(application: Application)
}
