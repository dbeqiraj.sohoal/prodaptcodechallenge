package com.dbeqiraj.prodaptkit.initializers

import android.app.Application
import com.dbeqiraj.prodaptkit.BuildConfig
import com.dbeqiraj.prodaptkit.initializers.base.AppInitializer
import timber.log.Timber
import javax.inject.Inject

class TimberInitializer @Inject constructor() : AppInitializer {
    override fun init(application: Application) {
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }
}
