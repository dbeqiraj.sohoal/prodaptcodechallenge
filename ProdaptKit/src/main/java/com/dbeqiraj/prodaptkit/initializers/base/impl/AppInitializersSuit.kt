package com.dbeqiraj.prodaptkit.initializers.base.impl

import android.app.Application
import com.dbeqiraj.prodaptkit.initializers.base.AppInitializer
import timber.log.Timber
import javax.inject.Inject

class AppInitializersSuit @Inject constructor(
    private val initializers: Set<@JvmSuppressWildcards AppInitializer>
) {
    fun init(application: Application) {
        initializers.forEach {
            Timber.d("AppInitializers::: initializing:[$it]")
            it.init(application)
        }
    }
}
