package com.dbeqiraj.prodaptkit.location

import android.Manifest
import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationManager
import android.os.HandlerThread
import android.os.Looper
import android.provider.Settings
import androidx.core.app.ActivityCompat
import com.dbeqiraj.prodaptkit.R
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationResult
import com.google.android.gms.location.LocationServices

class LocationServiceHandler : LocationDelegate {

    private var context: Context? = null
    private var location: Location? = null

    // Only ask 1 time per session
    private var alreadyAskedForGPS: Boolean = false

    @SuppressLint("MissingPermission")
    override fun requestLocationUpdate(context: Context) {
        this.context = context
        // If there is not permission granted skip this
        if (hasLocationPermissions(context)) {
            return
        }
        // Check if GPS is enabled
        if (!isGPSEnabled() && !alreadyAskedForGPS) {
            alreadyAskedForGPS = true
            buildAlertMessageNoGps()
            return
        }
        // Build the request
        val locationRequest = LocationRequest.create().apply {
            priority = LocationRequest.PRIORITY_HIGH_ACCURACY
            interval = 20 * 1000
        }
        // Loop handler
        val handlerThread = HandlerThread("MyHandlerThread")
        handlerThread.start()
        val looper: Looper = handlerThread.looper

        val mFusedLocationClient = LocationServices.getFusedLocationProviderClient(context)

        // Callback
        val locationCallback = object : LocationCallback() {
            override fun onLocationResult(result: LocationResult) {
                location = result.lastLocation
                // 1 update per session is enough for this code challenge
                mFusedLocationClient.removeLocationUpdates(this)
            }
        }
        // Request update
        mFusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, looper)
    }

    override fun getLastKnownLocation(): Location? = location

    // region - Private funs

    private fun hasLocationPermissions(context: Context) = ActivityCompat.checkSelfPermission(
        context,
        Manifest.permission.ACCESS_FINE_LOCATION
    ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
        context,
        Manifest.permission.ACCESS_COARSE_LOCATION
    ) != PackageManager.PERMISSION_GRANTED

    private fun isGPSEnabled(): Boolean {
        val manager = context?.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        return manager.isProviderEnabled(LocationManager.GPS_PROVIDER)
    }

    private fun buildAlertMessageNoGps() {
        val builder: AlertDialog.Builder = AlertDialog.Builder(context).apply {
            setMessage(context.getString(R.string.settings_permissions_ask))
                .setCancelable(false)
                .setPositiveButton(
                    context.getString(R.string.settings_permissions_button_yes)
                ) { _, _ -> context.startActivity(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)) }
                .setNegativeButton(
                    context.getString(R.string.settings_permissions_button_no)
                ) { dialog, _ -> dialog.cancel() }
        }
        val alert: AlertDialog = builder.create()
        alert.show()
    }

    // region
}
