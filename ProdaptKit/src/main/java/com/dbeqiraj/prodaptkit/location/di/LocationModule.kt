package com.dbeqiraj.prodaptkit.location.di

import com.dbeqiraj.prodaptkit.location.LocationDelegate
import com.dbeqiraj.prodaptkit.location.LocationServiceHandler
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object LocationModule {

    @Provides
    @Singleton
    fun provideLocationDelegate(): LocationDelegate = LocationServiceHandler()
}
