package com.dbeqiraj.prodaptkit.location

import android.content.Context
import android.location.Location

interface LocationDelegate {
    fun requestLocationUpdate(context: Context)
    fun getLastKnownLocation(): Location?
}
