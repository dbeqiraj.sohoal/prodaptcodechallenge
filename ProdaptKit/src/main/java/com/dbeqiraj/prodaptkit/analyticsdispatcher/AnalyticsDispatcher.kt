package com.dbeqiraj.prodaptkit.analyticsdispatcher

import com.dbeqiraj.prodaptkit.analyticsdispatcher.events.CustomEvent
import com.dbeqiraj.prodaptkit.analyticsdispatcher.events.base.Event
import com.dbeqiraj.prodaptkit.analyticsdispatcher.exceptions.UnsupportedEventException

/**
 * AnalyticsDispatcher is an interface that should be implemented for every analytics service.
 * For example: FirebaseDispatcherImpl or AnswersDispatcherImpl
 *
 * @property init - initDispatcher will be called only if this property is set to *true*
 * @property kit - should be represented by a singleton of a class that extends @AnalyticsKit
 */
interface AnalyticsDispatcher {

    val init: Boolean

    val kit: AnalyticsKit

    val dispatcherName: String

    /**
     * Should call the analytics library's initiation methods
     */
    fun initDispatcher()

    // region - Tracking methods

    /**
     * All tracking methods below are not implemented by default.
     * Each specific DispatcherImplementer should override the tracking methods it needs.
     */

    fun trackCustomEvent(event: CustomEvent) {
        throw NotImplementedError("$dispatcherName: trackCustomEvent is not implemented for this dispatcher")
    }

    //endregion

    /**
     * This method is called from the parent @Analytics class for each event.
     * Override this method if you plan on interfacing your own event types.
     */
    fun track(event: Event) {
        // track the event only if it is not configured as excluded
        if (event.isConsideredIncluded(kit)) {
            var handled = false

            // track for each type differently, including multiple implementations
            if (event is CustomEvent) {
                trackCustomEvent(event)
                handled = true
            }

            if (!handled) {
                throw UnsupportedEventException(event)
            }
        }
    }
}
