package com.dbeqiraj.prodaptkit.analyticsdispatcher.exceptions

import com.dbeqiraj.prodaptkit.analyticsdispatcher.events.base.Event

class UnsupportedEventException(event: Event) : UnsupportedOperationException() {

    override val message: String = "couldn't fire \"${event.javaClass.name}\" event"
}
