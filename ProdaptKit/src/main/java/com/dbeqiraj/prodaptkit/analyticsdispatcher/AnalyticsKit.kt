package com.dbeqiraj.prodaptkit.analyticsdispatcher

/**
 * Represents a service, such as Answers or Firebase
 */
interface AnalyticsKit {

    val name: String
}
