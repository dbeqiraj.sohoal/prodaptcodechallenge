package com.dbeqiraj.prodaptkit.helpers

import com.google.android.gms.location.ActivityRecognition
import timber.log.Timber
import java.util.*
import javax.mail.*
import javax.mail.internet.InternetAddress
import javax.mail.internet.MimeBodyPart
import javax.mail.internet.MimeMessage
import javax.mail.internet.MimeMultipart

class MailServiceProvider(
    private val host: String = DEFAULT_HOST,
    private val port: String = DEFAULT_PORT
) {
    data class Email(
        val auth: Authenticator,
        val toList: List<String>,
        val from: String,
        val subject: String,
        val body: String
    )

    class UserPassAuthenticator(private val username: String, private val password: String) :
        Authenticator() {
        override fun getPasswordAuthentication(): PasswordAuthentication {
            return PasswordAuthentication(username, password)
        }
    }

    fun send(email: Email) {
        try {
            val from = InternetAddress(email.from)
            val recipients = email.toList.map { InternetAddress(it) }

            val props = Properties()
            props["mail.smtp.auth"] = "true"
            props["mail.user"] = from
            props["mail.smtp.host"] = host
            props["mail.smtp.port"] = port
            props["mail.smtp.starttls.enable"] = "true"
            props["mail.smtp.ssl.trust"] = host
            props["mail.mime.charset"] = "UTF-8"
            val msg: Message = MimeMessage(Session.getDefaultInstance(props, email.auth))
            msg.setFrom(from)
            msg.sentDate = Calendar.getInstance().time
            msg.setRecipients(Message.RecipientType.TO, recipients.toTypedArray())
            msg.replyTo = arrayOf(from)

            msg.addHeader("X-Mailer", ActivityRecognition.CLIENT_NAME)
            msg.addHeader("Precedence", "bulk")
            msg.subject = email.subject

            msg.setContent(
                MimeMultipart().apply {
                    addBodyPart(
                        MimeBodyPart().apply {
                            setText(email.body, "iso-8859-1")
                        }
                    )
                }
            )
            Transport.send(msg)
        } catch (ex: Exception) {
            Timber.e(ex)
        }
    }

    companion object {
        private const val DEFAULT_HOST = "smtp.office365.com"
        private const val DEFAULT_PORT = "587"
    }
}
