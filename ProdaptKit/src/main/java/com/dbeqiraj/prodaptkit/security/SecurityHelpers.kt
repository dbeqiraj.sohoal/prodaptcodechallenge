package com.dbeqiraj.prodaptkit

import java.security.MessageDigest

// Supported Algorithms
const val MD5 = "MD5"
const val SHA256 = "SHA-256"

fun hashString(input: String, algorithm: String): String {
    return MessageDigest.getInstance(algorithm)
        .digest(input.toByteArray())
        .fold("") { str, it -> str + "%02x".format(it) }
}
