package com.dbeqiraj.prodaptkit.utils

object Constants {
    const val PREFERRED_IMAGE_WIDTH = 600
    const val PREFERRED_IMAGE_HEIGHT = 600
}
