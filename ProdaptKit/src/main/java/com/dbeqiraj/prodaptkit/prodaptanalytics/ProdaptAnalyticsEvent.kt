package com.dbeqiraj.prodaptkit.prodaptanalytics

data class ProdaptAnalyticsEvent(
    private val appId: String,
    private val action: String,
    private val resourceId: String,
    private val userId: String,
    private val meta: Meta
) {
    data class Meta(
        private val timestamp: Long,
        private val location: Location
    ) {
        data class Location(
            private val lat: String?,
            private val long: String?
        )
    }
}
