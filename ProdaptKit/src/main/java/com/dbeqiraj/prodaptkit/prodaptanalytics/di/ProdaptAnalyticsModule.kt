package com.dbeqiraj.prodaptkit.prodaptanalytics.di

import com.dbeqiraj.prodaptkit.prodaptanalytics.ProdaptAnalytics
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object ProdaptAnalyticsModule {

    @Provides
    @Singleton
    fun provideProdaptAnalytics(): ProdaptAnalytics = ProdaptAnalytics
}
