package com.dbeqiraj.prodaptkit.prodaptanalytics

import com.google.gson.Gson
import com.google.gson.GsonBuilder

/**
 * This is a custom analytics kit.
 * The same logic can extended to any other analytics api: Firebase, Adjust, AppMetrica etc.
 */
object ProdaptAnalytics {

    /**
     * Analytics pool
     */
    private val eventsPool: MutableList<ProdaptAnalyticsEvent> = mutableListOf()

    /**
     * Add event to the pool
     */
    fun logEvent(event: ProdaptAnalyticsEvent) {
        eventsPool.add(event)
    }

    /**
     * Clear all events from the pool
     */
    fun clearEvents() {
        eventsPool.clear()
    }

    /**
     * Get all events from the pool
     */
    fun getEvents() = eventsPool

    /**
     * Get events as json
     */
    fun getPrettyEvents(): String {
        val gson: Gson = GsonBuilder().setPrettyPrinting().create()
        return gson.toJson(eventsPool)
    }
}
