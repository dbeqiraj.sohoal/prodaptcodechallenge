/*
 *     Created by Dionis Beqiraj on 10/08/2022, 16:42
 *     dbeqiraj.sohoal@gmail.com
 *     Last modified 10/08/2022, 16:41
 *     Copyright (c) 2022.
 *     All rights reserved.
 */

package com.dbeqiraj.annotiations_processor.paginate

import com.dbeqiraj.annotiations.Paginate
import com.dbeqiraj.annotiations_processor._common.KAPT_KOTLIN_GENERATED_OPTION_NAME
import com.dbeqiraj.annotiations_processor._common.getDaoName
import com.dbeqiraj.annotiations_processor._common.getEntityName
import com.dbeqiraj.annotiations_processor._common.writeFile
import com.dbeqiraj.annotiations_processor.paginate.remotekeydao.RemoteKeyDaoBuilder
import com.dbeqiraj.annotiations_processor.paginate.remotekeyentity.RemoteKeyEntityBuilder
import com.google.auto.service.AutoService
import javax.annotation.processing.*
import javax.lang.model.SourceVersion
import javax.lang.model.element.TypeElement

@AutoService(Processor::class)
@SupportedSourceVersion(SourceVersion.RELEASE_8)
@SupportedOptions(KAPT_KOTLIN_GENERATED_OPTION_NAME)
class PaginateGenerator : AbstractProcessor() {

    override fun getSupportedAnnotationTypes(): MutableSet<String> {
        return mutableSetOf(Paginate::class.java.name)
    }

    override fun getSupportedSourceVersion(): SourceVersion {
        return SourceVersion.latest()
    }

    override fun process(
        set: MutableSet<out TypeElement>?,
        roundEnvironment: RoundEnvironment?
    ): Boolean {
        roundEnvironment?.getElementsAnnotatedWith(Paginate::class.java)
            ?.forEach {
                val className = it.simpleName.toString()
                val pack = processingEnv.elementUtils.getPackageOf(it).toString() + ".pagination"
                generateClass(className, pack)
            }
        return true
    }

    private fun generateClass(className: String, pack: String) {
        // Create entity class
        val entityFileName = getEntityName(className)
        val entityFileContent = RemoteKeyEntityBuilder(entityFileName, pack).getContent()
        writeFile(entityFileName, entityFileContent, processingEnv)

        // Create dao class
        val daoFileName = getDaoName(className)
        val daoFileContent = RemoteKeyDaoBuilder(daoFileName, entityFileName, pack).getContent()
        writeFile(daoFileName, daoFileContent, processingEnv)
    }
}
