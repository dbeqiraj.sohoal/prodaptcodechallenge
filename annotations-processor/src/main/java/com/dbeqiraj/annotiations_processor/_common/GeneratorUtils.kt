/*
 *     Created by Dionis Beqiraj on 10/08/2022, 16:42
 *     dbeqiraj.sohoal@gmail.com
 *     Last modified 10/08/2022, 16:41
 *     Copyright (c) 2022.
 *     All rights reserved.
 */

package com.dbeqiraj.annotiations_processor._common

import java.io.File
import javax.annotation.processing.ProcessingEnvironment

const val KAPT_KOTLIN_GENERATED_OPTION_NAME = "kapt.kotlin.generated"

internal fun getEntityName(entity: String): String = "${entity}RemoteKey"

internal fun getDaoName(entity: String): String = "${entity}RemoteKeyDao"

internal fun getDatastoreSerializableName(entity: String): String = "${entity}Serializer"

internal fun writeFile(fileName: String, content: String, processingEnv: ProcessingEnvironment) {
    val kaptKotlinGeneratedDir = processingEnv.options[KAPT_KOTLIN_GENERATED_OPTION_NAME]
    val file = File(kaptKotlinGeneratedDir, "$fileName.kt")

    file.writeText(content)
}
