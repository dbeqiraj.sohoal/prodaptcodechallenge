typealias dep = com.dbeqiraj.internal.Dependencies
typealias and = com.dbeqiraj.internal.Android

plugins {
    id("java-library")
    id("org.jetbrains.kotlin.jvm")
    id("internal")

    kotlin("kapt")
}

dependencies {

    dep.libraries.apply {
        implementation(project(mapOf("path" to annotations)))
        compileOnly(project(mapOf("path" to annotations)))
    }

    dep.annotations.apply {
        implementation(autoService)
        kapt(autoService)
        implementation(kotlinStdLib)
    }
}

java {
    sourceCompatibility = JavaVersion.VERSION_11
    targetCompatibility = JavaVersion.VERSION_11
}