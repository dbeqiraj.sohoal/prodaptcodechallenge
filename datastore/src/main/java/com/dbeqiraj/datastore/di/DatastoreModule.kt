/*
 *     Created by Dionis Beqiraj on 10/08/2022, 16:42
 *     dbeqiraj.sohoal@gmail.com
 *     Last modified 10/08/2022, 16:41
 *     Copyright (c) 2022.
 *     All rights reserved.
 */

package com.dbeqiraj.datastore.di

import android.content.Context
import androidx.datastore.dataStore
import com.dbeqiraj.datastore.di.qualifiers.PostsPrefs
import com.dbeqiraj.datastore.prefs.entity.EntityPrefs
import com.dbeqiraj.datastore.prefs.entity.EntityPrefsSerializer
import com.dbeqiraj.datastore.prefs.settings.SettingsPrefs
import com.dbeqiraj.datastore.prefs.settings.SettingsPrefsSerializer
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object DatastoreModule {

    // region - Initializers

    private val Context.entityPrefs by dataStore(
        fileName = classNameToJsonFileName<EntityPrefs>(),
        serializer = EntityPrefsSerializer
    )

    private val Context.settingsPrefs by dataStore(
        fileName = classNameToJsonFileName<SettingsPrefs>(),
        serializer = SettingsPrefsSerializer
    )

    // endregion

    // region - Providers

    @Provides
    @Singleton
    @PostsPrefs
    fun providePostsPrefs(@ApplicationContext context: Context) = context.entityPrefs

    @Provides
    @Singleton
    fun provideSettingsPrefs(@ApplicationContext context: Context) = context.settingsPrefs

    // endregion

    // region - Private methods

    private inline fun <reified T> classNameToJsonFileName(): String {
        return T::class.java.name.mapIndexed { index, char ->
            when {
                index == 0 -> char.lowercase()
                char.isUpperCase() -> "-${char.lowercase()}"
                else -> char
            }
        }.joinToString(
            separator = "",
            postfix = ".json"
        )
    }

    // endregion
}
