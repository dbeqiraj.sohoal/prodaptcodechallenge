/*
 *     Created by Dionis Beqiraj on 10/08/2022, 16:42
 *     dbeqiraj.sohoal@gmail.com
 *     Last modified 10/08/2022, 16:41
 *     Copyright (c) 2022.
 *     All rights reserved.
 */

package com.dbeqiraj.datastore.prefs.entity

import androidx.datastore.core.Serializer
import kotlinx.serialization.SerializationException
import kotlinx.serialization.json.Json
import timber.log.Timber
import java.io.InputStream
import java.io.OutputStream

@Suppress("BlockingMethodInNonBlockingContext")
object EntityPrefsSerializer : Serializer<EntityPrefs> {

    override val defaultValue: EntityPrefs
        get() = EntityPrefs()

    override suspend fun readFrom(input: InputStream): EntityPrefs {
        return try {
            Json.decodeFromString(
                deserializer = EntityPrefs.serializer(),
                string = input.readBytes().decodeToString()
            )
        } catch (e: SerializationException) {
            Timber.e(e)
            defaultValue
        }
    }

    override suspend fun writeTo(t: EntityPrefs, output: OutputStream) {
        output.write(
            Json.encodeToString(
                serializer = EntityPrefs.serializer(),
                value = t
            ).encodeToByteArray()
        )
    }
}
