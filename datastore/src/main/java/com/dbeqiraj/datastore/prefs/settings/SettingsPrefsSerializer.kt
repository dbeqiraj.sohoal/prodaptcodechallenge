package com.dbeqiraj.datastore.prefs.settings

import androidx.datastore.core.Serializer
import kotlinx.serialization.SerializationException
import kotlinx.serialization.json.Json
import timber.log.Timber
import java.io.InputStream
import java.io.OutputStream

@Suppress("BlockingMethodInNonBlockingContext")
object SettingsPrefsSerializer : Serializer<SettingsPrefs> {

    override val defaultValue: SettingsPrefs
        get() = SettingsPrefs()

    override suspend fun readFrom(input: InputStream): SettingsPrefs {
        return try {
            Json.decodeFromString(
                deserializer = SettingsPrefs.serializer(),
                string = input.readBytes().decodeToString()
            )
        } catch (e: SerializationException) {
            Timber.e(e)
            defaultValue
        }
    }

    override suspend fun writeTo(t: SettingsPrefs, output: OutputStream) {
        output.write(
            Json.encodeToString(
                serializer = SettingsPrefs.serializer(),
                value = t
            ).encodeToByteArray()
        )
    }
}
