package com.dbeqiraj.datastore.prefs.settings

import kotlinx.serialization.Serializable

@Serializable
data class SettingsPrefs(
    val email: String = ""
)
