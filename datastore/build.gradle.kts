typealias and = com.dbeqiraj.internal.Android
typealias dep = com.dbeqiraj.internal.Dependencies

plugins {
    id("com.android.library")
    id("kotlin-android")
    id("internal")

    kotlin("kapt")
    kotlin("plugin.serialization")
}

android {

    compileSdk = and.compileSdk

    defaultConfig {

        minSdk = and.minSdk
        targetSdk = and.targetSdk

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }
    buildTypes {
        release {
            isMinifyEnabled = false
            proguardFiles(getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro")
        }
    }

}

dependencies {

    dep.datastore.apply { // https://developer.android.com/topic/libraries/architecture/datastore
        implementation(datastore)
    }

    dep.hilt.apply { // https://dagger.dev/hilt/
        implementation(hiltAndroid)
        kapt(hiltCompiler)
        kapt(daggerHiltCompiler)
    }

    dep.other.apply { // Miscellaneous required libraries
        implementation(serialization)
    }

    dep.libraries.apply { // Internal libraries
        implementation(project(prodaptKit))
    }

}
