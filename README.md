- **Features**
1. First screen shows a list of `Post` entities from https://jsonplaceholder.typicode.com/posts. Each item contains title, description, author, the number of comments etc. The author and the comments are fetched from two different endpoints: https://jsonplaceholder.typicode.com/users/[id] and https://jsonplaceholder.typicode.com/posts/[post_id]/comments
2. On clicking a post it will lead to the details page showing some data about the post, the author, a location in maps and the comments for the given post.
3. There is a `Settings` where you can enter an email that will be used to send some events collected during the app usage.
4. The app tracks and collects some actions such as post click, pagination, list scroll. Each event has the following schema:

```
{
    "action": "view|scroll|page",
    "appId": "<my_email>",
    "meta": {
        "location": {
        "lat": "<lat | null>",
        "long": "<long | null>"
        },
        "timestamp": <time>
    },
    "resourceId": "<post_id>",
    "userId": "<device_id>"
}
```
5. When the app is sent to background or closed, the list with the collected events is sent to the email set in the `Settings` screen. (See `EmailWorker` class)

- **Implementation**
1. The ui is written completely in Jetpack Compose
1. For pagingating the posts list I make use of Paging 3 api (`PostsRemoteMediator`). The `posts` are fetched and stored gradually in the local Room database. Each page has 15 items and the data are consided expired after 1h. These params can be found in `data/utils/PagingConstants`.
1. For preferences I use proto datastore api
1. `EmailWorker` is the responsibale worker to send the collected events via email when app is closed
1. There are some unit and ui tests for the viewmodels, screens and the remote mediator
1. The formatting of the code is done with Ktlint


- **The tech stack**

1. Hilt
1. Retrofit
1. Jetpack Compose
1. Coil
1. Datastore
1. Material Design
1. Room
1. Paging 3
1. Google Maps
1. Espresso
1. Mockk
1. Ktlint

**Modules/Libraries**
1. *app* - This is where the ui lives (Jetpack Compose, ViewModels, navigation, theme, analytics events). It is on top of all the other layers (Data, Domain, Datastore). The ViewModels provide data wrapped with Kotlin Flows to the Composables.
2. *data* - This is where the data access logic takes place. This layer hides the data sources from the `app` layer.
Here we can decide to fetch data from remote or from local source.
This layer implements the actions for each repository defined in `domain` layer and is responsible to handle the success and error cases for the given operation.
3. *domain* - In `domain` the models are defined. The interfaces written in this layer describe the actions that we expect to perform on a given repository and each action corresponds to a single use case.
5. *datastore* - I put all the code related to datastore in this library. Using DI it is exposed to other layers. The idea is to have a separated module for storing user prefs and keep the implementation hidden from the other layers. For example, in case we will switch back to the traditional `SharedPrefs`, or we use another api for prefs, these changes should be transparent to the other layours. Unfortunately, given the limited time of 1 week, I couldn't decouple this module from Datastore (create abstraction layers over it).
6. *annotations* and *annotations-processor* - I like to create my own annotations to reduce repetitive work and boiler plate code. In this code challenge I created `@Paginate` that can be used to mark a data class for the purpose of generating its corresponding module that is needed in `RemoteMediator` when doing the pagination (to store the previous and next keys for each page). So, whenever we need to paginate a model, this annotation will automatically create the corresponding `RemoteKey` room entity and a `DAO`. See https://developer.android.com/reference/kotlin/androidx/paging/RemoteMediator
4. *ProdaptKit* - This is a bouquet of low-end utilities, initializers and abstract objects. It is NOT coupled with any of the other libraries (there is no internal library injection in its `build.gradle.kts`), because it is created for the intention of being reusable by other projects in the company.
8. *Internal* - A reusable module that holds the most common dependencies needed for a native Android application.

***Secret keys***

There is this `secrets.properties` at the root of the project. 
- It holds a `apiKey` needed for Google Maps (you can use the one that I created).
- I created a dump account in Outlook needed to send the events via email. You can put your own credentials and play around with it.


