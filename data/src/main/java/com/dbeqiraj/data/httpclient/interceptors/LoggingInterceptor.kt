/*
 *     Created by Dionis Beqiraj on 10/08/2022, 16:42
 *     dbeqiraj.sohoal@gmail.com
 *     Last modified 10/08/2022, 16:41
 *     Copyright (c) 2022.
 *     All rights reserved.
 */

package com.dbeqiraj.data.httpclient.interceptors

import com.dbeqiraj.data.BuildConfig
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.internal.platform.Platform
import okhttp3.logging.HttpLoggingInterceptor
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
internal object LoggingInterceptor {

    @Provides
    @Singleton
    fun provideHttpLoggingInterceptor(
        logger: HttpLoggingInterceptor.Logger,
        level: HttpLoggingInterceptor.Level
    ): HttpLoggingInterceptor {
        val httpLoggingInterceptor = HttpLoggingInterceptor(logger)
        return httpLoggingInterceptor.apply { httpLoggingInterceptor.level = level }
    }

    @Provides
    @Singleton
    fun provideHttpLoggingInterceptorLogger(): HttpLoggingInterceptor.Logger {
        return HttpLoggingInterceptor.Logger { message ->
            Platform.get().log(message, Platform.INFO)
        }
    }

    @Provides
    @Singleton
    fun provideHttpLoggingLevel(): HttpLoggingInterceptor.Level = if (BuildConfig.DEBUG) {
        HttpLoggingInterceptor.Level.BODY
    } else {
        HttpLoggingInterceptor.Level.NONE
    }
}
