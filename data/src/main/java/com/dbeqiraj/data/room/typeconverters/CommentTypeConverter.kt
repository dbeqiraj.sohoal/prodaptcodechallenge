package com.dbeqiraj.data.room.typeconverters

import androidx.room.TypeConverter
import com.dbeqiraj.domain.models.comments.Comment
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.lang.reflect.Type

class CommentTypeConverter {

    @TypeConverter
    fun fromList(value: List<Comment>?) = Gson().toJson(value)

    @TypeConverter
    fun toList(data: String?): List<Comment>? {
        val listType: Type = object :
            TypeToken<List<Comment?>?>() {}.type
        return Gson().fromJson<List<Comment>?>(data, listType)
    }
}
