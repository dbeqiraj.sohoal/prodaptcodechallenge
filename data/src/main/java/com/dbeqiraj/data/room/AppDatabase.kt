/*
 *     Created by Dionis Beqiraj on 10/08/2022, 16:42
 *     dbeqiraj.sohoal@gmail.com
 *     Last modified 10/08/2022, 16:41
 *     Copyright (c) 2022.
 *     All rights reserved.
 */

package com.dbeqiraj.data.room

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.dbeqiraj.data.modules.posts.db.PostsDao
import com.dbeqiraj.data.room.typeconverters.CommentTypeConverter
import com.dbeqiraj.domain.models.posts.Post
import com.dbeqiraj.domain.models.posts.pagination.PostRemoteKey
import com.dbeqiraj.domain.models.posts.pagination.PostRemoteKeyDao

@Database(
    entities = [
        Post::class,
        PostRemoteKey::class
    ],
    version = 1,
    exportSchema = false
)
@TypeConverters(CommentTypeConverter::class)
abstract class AppDatabase : RoomDatabase() {
    // Posts
    abstract fun getPostsDao(): PostsDao
    abstract fun getPostsKeysDao(): PostRemoteKeyDao
}
