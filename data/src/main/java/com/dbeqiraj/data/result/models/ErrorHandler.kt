/*
 *     Created by Dionis Beqiraj on 10/08/2022, 16:42
 *     dbeqiraj.sohoal@gmail.com
 *     Last modified 10/08/2022, 16:41
 *     Copyright (c) 2022.
 *     All rights reserved.
 */

package com.dbeqiraj.data.result.models

import com.dbeqiraj.data.result.enums.ResponseErrorType

data class ErrorHandler(
    val type: ResponseErrorType,
    val message: String?
)
