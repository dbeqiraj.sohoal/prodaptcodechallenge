/*
 *     Created by Dionis Beqiraj on 10/08/2022, 16:42
 *     dbeqiraj.sohoal@gmail.com
 *     Last modified 10/08/2022, 16:41
 *     Copyright (c) 2022.
 *     All rights reserved.
 */

package com.dbeqiraj.data.result.enums

enum class ResponseErrorType {
    BAD_REQUEST,
    CONNECTION_ERROR,
    GENERIC_ERROR,
    UNAUTHORIZED,
    UNKNOWN_HOST
}
