/*
 *     Created by Dionis Beqiraj on 10/08/2022, 16:42
 *     dbeqiraj.sohoal@gmail.com
 *     Last modified 10/08/2022, 16:41
 *     Copyright (c) 2022.
 *     All rights reserved.
 */

package com.dbeqiraj.data.modules._common

import com.dbeqiraj.data.result.enums.ResponseErrorType
import com.dbeqiraj.data.result.models.ErrorHandler
import com.dbeqiraj.data.result.wrapper.ResponseResult
import retrofit2.Response
import java.net.SocketTimeoutException
import java.net.UnknownHostException

object BaseRepository {

    suspend fun <T : Any> safeApiCall(
        call: suspend () -> Response<T>
    ): ResponseResult<T> {
        try {
            val response = call.invoke()
            if (response.isSuccessful) {
                return ResponseResult.Success(response.body()!!)
            }

            val errorType = when (response.code()) {
                400,
                500 -> ResponseErrorType.BAD_REQUEST
                401 -> ResponseErrorType.UNAUTHORIZED
                else -> ResponseErrorType.GENERIC_ERROR
            }

            return ResponseResult.Error(ErrorHandler(errorType, response.message()))
        } catch (ex: Exception) {
            val errorType = when (ex) {
                is SocketTimeoutException -> ResponseErrorType.CONNECTION_ERROR
                is UnknownHostException -> ResponseErrorType.UNKNOWN_HOST
                else -> ResponseErrorType.GENERIC_ERROR
            }

            return ResponseResult.Error(ErrorHandler(errorType, ex.message))
        }
    }
}
