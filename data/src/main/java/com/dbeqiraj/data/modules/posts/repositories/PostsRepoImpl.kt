/*
 *     Created by Dionis Beqiraj on 10/08/2022, 16:42
 *     dbeqiraj.sohoal@gmail.com
 *     Last modified 10/08/2022, 16:41
 *     Copyright (c) 2022.
 *     All rights reserved.
 */

package com.dbeqiraj.data.modules.posts.repositories

import androidx.datastore.core.DataStore
import androidx.paging.ExperimentalPagingApi
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import com.dbeqiraj.data.modules.posts.api.PostsApiService
import com.dbeqiraj.data.modules.posts.paging.PostsRemoteMediator
import com.dbeqiraj.data.modules.users.api.UsersApiService
import com.dbeqiraj.data.room.AppDatabase
import com.dbeqiraj.data.utils.CoroutineDispatcherProvider
import com.dbeqiraj.data.utils.PagingConstants.PAGE_SIZE
import com.dbeqiraj.datastore.prefs.entity.EntityPrefs
import com.dbeqiraj.domain.models.posts.Post
import com.dbeqiraj.domain.repositories.PostsRepository
import kotlinx.coroutines.flow.Flow

@ExperimentalPagingApi
class PostsRepoImpl(
    private val dispatcherProvider: CoroutineDispatcherProvider,
    private val postsApiService: PostsApiService,
    private val usersApiService: UsersApiService,
    private val datastore: DataStore<EntityPrefs>,
    private val database: AppDatabase
) : PostsRepository {

    override fun getPosts(): Flow<PagingData<Post>> {
        val pagingSourceFactory = { database.getPostsDao().getAll() }

        return Pager(
            config = PagingConfig(
                pageSize = PAGE_SIZE
            ),
            remoteMediator = PostsRemoteMediator(
                dispatcherProvider,
                postsApiService,
                usersApiService,
                datastore,
                database
            ),
            pagingSourceFactory = pagingSourceFactory
        ).flow
    }

    override fun getPostById(id: Int): Flow<Post> = database.getPostsDao().getPostById(id)
}
