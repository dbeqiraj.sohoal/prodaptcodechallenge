package com.dbeqiraj.data.modules.users.api

import com.dbeqiraj.domain.models.users.User
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

interface UsersApiService {

    @GET("users/{id}")
    suspend fun getUserById(@Path("id") id: Int): Response<User>
}
