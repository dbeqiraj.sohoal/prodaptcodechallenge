package com.dbeqiraj.data.modules.users.repositories

import com.dbeqiraj.data.modules._common.BaseRepository.safeApiCall
import com.dbeqiraj.data.modules.users.api.UsersApiService
import com.dbeqiraj.data.result.wrapper.ResponseResult
import com.dbeqiraj.domain.models.users.User
import com.dbeqiraj.domain.repositories.UsersRepository

class UsersRepoImpl(
    private val apiService: UsersApiService
) : UsersRepository {

    override suspend fun getUserById(id: Int): Result<User> {
        val response = safeApiCall {
            apiService.getUserById(id)
        }

        return when (response) {
            is ResponseResult.Success -> Result.success(response.data)
            is ResponseResult.Error -> Result.failure(Throwable(response.error.message))
        }
    }
}
