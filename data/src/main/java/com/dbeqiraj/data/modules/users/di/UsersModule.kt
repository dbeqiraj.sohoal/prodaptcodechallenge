/*
 *     Created by Dionis Beqiraj on 10/08/2022, 16:42
 *     dbeqiraj.sohoal@gmail.com
 *     Last modified 10/08/2022, 16:41
 *     Copyright (c) 2022.
 *     All rights reserved.
 */

package com.dbeqiraj.data.modules.users.di

import androidx.paging.ExperimentalPagingApi
import com.dbeqiraj.data.modules.users.api.UsersApiService
import com.dbeqiraj.data.modules.users.repositories.UsersRepoImpl
import com.dbeqiraj.domain.repositories.UsersRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import javax.inject.Singleton

@ExperimentalPagingApi
@Module
@InstallIn(SingletonComponent::class)
internal object UsersModule {

    @Provides
    @Singleton
    fun provideUsersRepository(
        apiService: UsersApiService
    ): UsersRepository = UsersRepoImpl(apiService)

    @Provides
    @Singleton
    fun provideUsersApiService(retrofit: Retrofit): UsersApiService {
        return retrofit.create(UsersApiService::class.java)
    }
}
