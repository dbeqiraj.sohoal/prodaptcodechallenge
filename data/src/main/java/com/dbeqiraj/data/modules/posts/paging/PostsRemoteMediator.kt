/*
 *     Created by Dionis Beqiraj on 10/08/2022, 16:42
 *     dbeqiraj.sohoal@gmail.com
 *     Last modified 10/08/2022, 16:41
 *     Copyright (c) 2022.
 *     All rights reserved.
 */

package com.dbeqiraj.data.modules.posts.paging

import androidx.datastore.core.DataStore
import androidx.paging.ExperimentalPagingApi
import androidx.paging.LoadType
import androidx.paging.PagingState
import androidx.paging.RemoteMediator
import androidx.room.withTransaction
import com.dbeqiraj.data.modules._common.BaseRepository.safeApiCall
import com.dbeqiraj.data.modules.posts.api.PostsApiService
import com.dbeqiraj.data.modules.users.api.UsersApiService
import com.dbeqiraj.data.result.wrapper.ResponseResult
import com.dbeqiraj.data.room.AppDatabase
import com.dbeqiraj.data.utils.CoroutineDispatcherProvider
import com.dbeqiraj.data.utils.PagingConstants
import com.dbeqiraj.data.utils.PagingConstants.PAGE_SIZE
import com.dbeqiraj.datastore.prefs.entity.EntityPrefs
import com.dbeqiraj.domain.models.posts.Post
import com.dbeqiraj.domain.models.posts.pagination.PostRemoteKey
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.async
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch
import timber.log.Timber

@ExperimentalPagingApi
class PostsRemoteMediator(
    private val dispatcherProvider: CoroutineDispatcherProvider,
    private val postsApiService: PostsApiService,
    private val usersApiService: UsersApiService,
    private val datastore: DataStore<EntityPrefs>,
    private val db: AppDatabase
) : RemoteMediator<Int, Post>() {

    override suspend fun initialize(): InitializeAction {
        val lastUpdatesPrefs = datastore.data.first()

        return if (System.currentTimeMillis() - lastUpdatesPrefs.lastUpdated >= PagingConstants.CACHE_TIMEOUT) {
            InitializeAction.LAUNCH_INITIAL_REFRESH
        } else {
            InitializeAction.SKIP_INITIAL_REFRESH
        }
    }

    override suspend fun load(
        loadType: LoadType,
        state: PagingState<Int, Post>
    ): MediatorResult {
        val pageKeyData = getKeyPageData(loadType, state)
        val page = when (pageKeyData) {
            is MediatorResult.Success -> {
                return pageKeyData
            }
            else -> {
                pageKeyData as Int
            }
        }

        return try {
            val response = safeApiCall {
                postsApiService.getPosts(page = page)
            }

            when (response) {
                is ResponseResult.Success -> {
                    val posts = response.data
                    val isEndOfList = posts.size < PAGE_SIZE
                    db.withTransaction {
                        if (loadType == LoadType.REFRESH) {
                            // Save the refresh time
                            datastore.updateData {
                                it.copy(
                                    lastUpdated = System.currentTimeMillis()
                                )
                            }
                            // Clean database when refreshing
                            db.getPostsDao().deleteAll()
                            db.getPostsKeysDao().deleteAll()
                        }
                        val prevKey = if (page == STARTING_PAGE_INDEX) null else page - 1
                        val nextKey = if (isEndOfList) null else page + 1
                        val keys = posts.map {
                            PostRemoteKey(it.id, prevKey = prevKey, nextKey = nextKey)
                        }
                        db.getPostsDao().insertAll(posts)
                        db.getPostsKeysDao().insertAll(keys)
                    }

                    posts.forEach { post ->
                        // Fill post with additional data
                        fetAdditionalData(post)
                    }
                    MediatorResult.Success(endOfPaginationReached = isEndOfList)
                }
                is ResponseResult.Error -> {
                    MediatorResult.Error(Exception(response.error.message))
                }
            }
        } catch (e: Exception) {
            MediatorResult.Error(e)
        }
    }

    // region - Private helper functions

    private suspend fun getKeyPageData(
        loadType: LoadType,
        state: PagingState<Int, Post>
    ): Any {
        return when (loadType) {
            LoadType.REFRESH -> {
                val remoteKeys = getRemoteKeyClosestToCurrentPosition(state)
                remoteKeys?.nextKey?.minus(1) ?: STARTING_PAGE_INDEX
            }
            LoadType.APPEND -> {
                val remoteKeys = getLastRemoteKey(state)
                val nextKey = remoteKeys?.nextKey
                nextKey
                    ?: return MediatorResult.Success(endOfPaginationReached = remoteKeys != null)
            }
            LoadType.PREPEND -> {
                val remoteKeys = getFirstRemoteKey(state)
                val prevKey = remoteKeys?.prevKey ?: return MediatorResult.Success(
                    endOfPaginationReached = remoteKeys != null
                )
                prevKey
            }
        }
    }

    private suspend fun getRemoteKeyClosestToCurrentPosition(state: PagingState<Int, Post>): PostRemoteKey? {
        return state.anchorPosition?.let { position ->
            state.closestItemToPosition(position)?.id?.let { repoId ->
                db.getPostsKeysDao().remoteKeysById(repoId)
            }
        }
    }

    private suspend fun getLastRemoteKey(state: PagingState<Int, Post>): PostRemoteKey? {
        return state.pages
            .lastOrNull { it.data.isNotEmpty() }
            ?.data?.lastOrNull()
            ?.let { Post -> db.getPostsKeysDao().remoteKeysById(Post.id) }
    }

    private suspend fun getFirstRemoteKey(state: PagingState<Int, Post>): PostRemoteKey? {
        return state.pages
            .firstOrNull { it.data.isNotEmpty() }
            ?.data?.firstOrNull()
            ?.let { Post -> db.getPostsKeysDao().remoteKeysById(Post.id) }
    }

    // endregion

    // region - additional data repos

    private suspend fun fetAdditionalData(post: Post) {
        try {
            CoroutineScope(dispatcherProvider.io).launch {
                // Launch request for user
                val userResponse = async { usersApiService.getUserById(post.userId) }.await()
                // Launch request for comments
                val commentsResponse = async { postsApiService.getCommentsByPost(post.userId) }.await()
                // Wait until both finish to only update 1 time in the database
                val user = when (userResponse.isSuccessful) {
                    true -> userResponse.body()
                    false -> {
                        val message = userResponse.message()
                            ?: "Unknown error while fetching user with id: ${post.userId}"
                        Timber.d("PostsRemoteMediator ::: Error: $message")
                        null
                    }
                }

                val comments = when (commentsResponse.isSuccessful) {
                    true -> commentsResponse.body()
                    false -> {
                        val message = commentsResponse.message()
                            ?: "Unknown error while fetching comments for post with id: ${post.userId}"
                        Timber.d("PostsRemoteMediator ::: Error: $message")
                        null
                    }
                }

                db.withTransaction {
                    db.getPostsDao().update(
                        post.copy(
                            user = user,
                            comments = comments
                        )
                    )
                }
            }
        } catch (ex: Exception) {
            Timber.d(ex)
        }
    }

    // endregion

    companion object {
        private const val STARTING_PAGE_INDEX = 1
    }
}
