/*
 *     Created by Dionis Beqiraj on 10/08/2022, 16:42
 *     dbeqiraj.sohoal@gmail.com
 *     Last modified 10/08/2022, 16:41
 *     Copyright (c) 2022.
 *     All rights reserved.
 */

package com.dbeqiraj.data.modules.posts.db

import androidx.paging.PagingSource
import androidx.room.*
import com.dbeqiraj.domain.models.posts.Post
import kotlinx.coroutines.flow.Flow

@Dao
interface PostsDao {

    @Query("SELECT * FROM Post ORDER BY id ASC")
    fun getAll(): PagingSource<Int, Post>

    @Query("SELECT * FROM Post WHERE id = :id")
    fun getPostById(id: Int): Flow<Post>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(posts: List<Post>)

    @Update
    suspend fun update(post: Post)

    @Query("DELETE FROM Post")
    suspend fun deleteAll()
}
