/*
 *     Created by Dionis Beqiraj on 10/08/2022, 16:42
 *     dbeqiraj.sohoal@gmail.com
 *     Last modified 10/08/2022, 16:41
 *     Copyright (c) 2022.
 *     All rights reserved.
 */

package com.dbeqiraj.data.modules.posts.di

import androidx.datastore.core.DataStore
import androidx.paging.ExperimentalPagingApi
import com.dbeqiraj.data.modules.posts.api.PostsApiService
import com.dbeqiraj.data.modules.posts.repositories.PostsRepoImpl
import com.dbeqiraj.data.modules.users.api.UsersApiService
import com.dbeqiraj.data.room.AppDatabase
import com.dbeqiraj.data.utils.CoroutineDispatcherProvider
import com.dbeqiraj.datastore.di.qualifiers.PostsPrefs
import com.dbeqiraj.datastore.prefs.entity.EntityPrefs
import com.dbeqiraj.domain.repositories.PostsRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import javax.inject.Singleton

@ExperimentalPagingApi
@Module
@InstallIn(SingletonComponent::class)
internal object PostsModule {

    @Provides
    @Singleton
    fun providePostsRepository(
        dispatcherProvider: CoroutineDispatcherProvider,
        postsApiService: PostsApiService,
        usersApiService: UsersApiService,
        @PostsPrefs datastore: DataStore<EntityPrefs>,
        database: AppDatabase
    ): PostsRepository =
        PostsRepoImpl(dispatcherProvider, postsApiService, usersApiService, datastore, database)

    @Provides
    @Singleton
    fun providePostsApiService(retrofit: Retrofit): PostsApiService {
        return retrofit.create(PostsApiService::class.java)
    }
}
