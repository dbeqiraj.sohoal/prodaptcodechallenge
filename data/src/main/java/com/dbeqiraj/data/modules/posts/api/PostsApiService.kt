/*
 *     Created by Dionis Beqiraj on 10/08/2022, 16:42
 *     dbeqiraj.sohoal@gmail.com
 *     Last modified 10/08/2022, 16:41
 *     Copyright (c) 2022.
 *     All rights reserved.
 */

package com.dbeqiraj.data.modules.posts.api

import androidx.annotation.IntRange
import com.dbeqiraj.data.utils.PagingConstants
import com.dbeqiraj.domain.models.comments.Comment
import com.dbeqiraj.domain.models.posts.Post
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface PostsApiService {

    @GET("posts")
    suspend fun getPosts(
        @Query("_page")
        page: Int = 1,
        @IntRange(from = 1, to = 50)
        @Query("_limit")
        perPage: Int = PagingConstants.PAGE_SIZE,
        @Query("_sort")
        sortBy: String = "id",
        @Query("_order")
        order: String = "asc"
    ): Response<List<Post>>

    @GET("posts/{id}")
    suspend fun getPostById(@Path("id") id: Int): Response<Post>

    @GET("posts/{id}/comments")
    suspend fun getCommentsByPost(@Path("id") id: Int): Response<List<Comment>>
}
