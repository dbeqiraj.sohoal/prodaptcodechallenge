import Build_gradle.and
import Build_gradle.dep
import org.jetbrains.kotlin.kapt3.base.Kapt.kapt
import org.jetbrains.kotlin.konan.properties.Properties

typealias and = com.dbeqiraj.internal.Android
typealias dep = com.dbeqiraj.internal.Dependencies

plugins {
    id("dagger.hilt.android.plugin")
    id("com.android.library")
    id("kotlin-android")
    id("internal")

    kotlin("kapt")
}

android {

    compileSdk = and.compileSdk

    defaultConfig {

        // Base url
        buildConfigField(
            type = "String",
            name = "BASE_URL",
            value = "\"https://jsonplaceholder.typicode.com/\""
        )

        minSdk = and.minSdk
        targetSdk = and.targetSdk

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }
    buildTypes {
        release {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }

}

dependencies {

    dep.compose.apply { // https://developer.android.com/jetpack/compose
        implementation(pagingCompose)
    }

    dep.datastore.apply { // https://developer.android.com/topic/libraries/architecture/datastore
        implementation(datastore)
    }

    dep.hilt.apply { // https://dagger.dev/hilt/
        implementation(hiltAndroid)
        kapt(hiltCompiler)
        kapt(daggerHiltCompiler)
    }

    dep.retrofit.apply { // https://square.github.io/retrofit/
        implementation(retrofit2)
        implementation(interceptor)
        implementation(converterMoshi)
    }

    dep.room.apply { // https://developer.android.com/jetpack/androidx/releases/room
        implementation(paging)
        implementation(runtime)
        implementation(ktx)
        kapt(compiler)
    }

    dep.other.apply { // Miscellaneous required libraries
        implementation(moshiLazyAdapter)
        implementation(kotlinxCoroutines)
        implementation(serialization)
        implementation(gson)
    }

    dep.libraries.apply { // Internal libraries
        implementation(project(prodaptKit))
        implementation(project(domain))
        implementation(project(datastore))
    }
}
