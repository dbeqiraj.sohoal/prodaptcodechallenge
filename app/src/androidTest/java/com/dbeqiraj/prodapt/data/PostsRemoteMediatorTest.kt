package com.dbeqiraj.prodapt.data

import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.test.junit4.createAndroidComposeRule
import androidx.datastore.core.DataStore
import androidx.paging.*
import androidx.room.Room
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import com.dbeqiraj.data.modules.posts.paging.PostsRemoteMediator
import com.dbeqiraj.data.modules.users.api.UsersApiService
import com.dbeqiraj.data.room.AppDatabase
import com.dbeqiraj.data.utils.CoroutineDispatcherProvider
import com.dbeqiraj.datastore.prefs.entity.EntityPrefs
import com.dbeqiraj.domain.models.posts.Post
import com.dbeqiraj.prodapt.MainActivity
import com.dbeqiraj.prodapt.utils.ApiAbstract
import com.google.accompanist.pager.ExperimentalPagerApi
import io.mockk.MockKAnnotations
import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.unmockkAll
import junit.framework.Assert.assertTrue
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import kotlin.time.ExperimentalTime

@ExperimentalPagerApi
@ExperimentalComposeUiApi
@ExperimentalPagingApi
@ExperimentalCoroutinesApi
@ExperimentalTime
@RunWith(AndroidJUnit4::class)
class PostsRemoteMediatorTest : ApiAbstract() {

    /**
     * App context
     */
    val appContext = InstrumentationRegistry.getInstrumentation().targetContext

    @get:Rule
    val composeTestRule = createAndroidComposeRule<MainActivity>()

    @RelaxedMockK
    lateinit var dispatcherProvider: CoroutineDispatcherProvider

    @RelaxedMockK
    lateinit var usersApiService: UsersApiService

    @RelaxedMockK
    lateinit var datastore: DataStore<EntityPrefs>

    private lateinit var db: AppDatabase

    private lateinit var remoteMediator: PostsRemoteMediator

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
        // Create room db
        db = Room.inMemoryDatabaseBuilder(
            appContext,
            AppDatabase::class.java
        ).build()
    }

    @After
    override fun tearDown() {
        super.tearDown()
        unmockkAll()
    }

    @Test
    fun testLoadFunction() = runBlockingApi(listOf(Post.mock()), 200) {
        // Initialize the RemoteMediator
        remoteMediator = PostsRemoteMediator(
            dispatcherProvider,
            postsApiService,
            usersApiService,
            datastore,
            db
        )
        val pagingState = PagingState<Int, Post>(
            listOf(),
            null,
            PagingConfig(3),
            0
        )

        // Test the load function
        val result = remoteMediator.load(LoadType.REFRESH, pagingState)
        // Success expected (code 200)
        assertTrue(result is RemoteMediator.MediatorResult.Success)
        assertTrue((result as RemoteMediator.MediatorResult.Success).endOfPaginationReached)
    }

    @Test
    fun testMediatorError() = runBlockingApi(null, 500) {
        // Initialize the RemoteMediator
        remoteMediator = PostsRemoteMediator(
            dispatcherProvider,
            postsApiService,
            usersApiService,
            datastore,
            db
        )
        val pagingState = PagingState<Int, Post>(
            listOf(),
            null,
            PagingConfig(3),
            0
        )

        // Test the load function
        val result = remoteMediator.load(LoadType.REFRESH, pagingState)
        // Error expected (code 500)
        assertTrue(result is RemoteMediator.MediatorResult.Error)
    }
}
