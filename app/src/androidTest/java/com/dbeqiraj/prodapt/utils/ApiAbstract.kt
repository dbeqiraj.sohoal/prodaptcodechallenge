/*
 * Copyright 2021 Vitaliy Zarubin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.dbeqiraj.prodapt.utils

import com.dbeqiraj.data.httpclient.HttpClientModule
import com.dbeqiraj.data.modules.posts.api.PostsApiService
import com.google.gson.Gson
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.runBlocking
import okhttp3.OkHttpClient
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.concurrent.TimeUnit

@RunWith(JUnit4::class)
abstract class ApiAbstract {

    private val gson = Gson()
    private val mockWebServer = MockWebServer()

    private val client = OkHttpClient.Builder()
        .connectTimeout(15, TimeUnit.SECONDS)
        .readTimeout(15, TimeUnit.SECONDS)
        .writeTimeout(15, TimeUnit.SECONDS)
        .build()

    private val retrofit = Retrofit.Builder()
        .baseUrl(mockWebServer.url("/"))
        .addConverterFactory(MoshiConverterFactory.create(HttpClientModule.provideMoshi()))
        .client(client)
        .build()

    val postsApiService = retrofit.create(PostsApiService::class.java)

    fun <T> runBlockingApi(mocked: Any?, code: Int, block: suspend CoroutineScope.() -> T) {
        mockWebServer.enqueue(
            MockResponse()
                .setResponseCode(code)
                .setBody(gson.toJson(mocked))
        )
        runBlocking {
            block.invoke(this)
        }
    }

    @After
    open fun tearDown() {
        mockWebServer.shutdown()
    }
}
