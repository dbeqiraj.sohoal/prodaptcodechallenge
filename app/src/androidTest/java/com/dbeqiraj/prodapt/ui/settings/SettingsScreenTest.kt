package com.dbeqiraj.prodapt.ui.settings

import androidx.compose.material.Surface
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.test.assertIsDisplayed
import androidx.compose.ui.test.junit4.createAndroidComposeRule
import androidx.compose.ui.test.onNodeWithText
import androidx.compose.ui.test.performClick
import androidx.paging.ExperimentalPagingApi
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.dbeqiraj.datastore.prefs.settings.SettingsPrefs
import com.dbeqiraj.prodapt.MainActivity
import com.dbeqiraj.prodapt.R
import com.dbeqiraj.prodapt.theme.MyTheme
import com.dbeqiraj.prodapt.ui.settings.ui.screens.SettingsBody
import com.dbeqiraj.prodapt.ui.settings.ui.viewmodels.SettingsViewModel
import com.dbeqiraj.prodapt.utils.ToastMatcher
import com.google.accompanist.pager.ExperimentalPagerApi
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.unmockkAll
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import kotlin.time.ExperimentalTime

@ExperimentalPagerApi
@ExperimentalComposeUiApi
@ExperimentalPagingApi
@ExperimentalCoroutinesApi
@ExperimentalTime
@RunWith(AndroidJUnit4::class)
class SettingsScreenTest {

    @get:Rule
    val composeTestRule = createAndroidComposeRule<MainActivity>()

    @RelaxedMockK
    lateinit var settingsViewModel: SettingsViewModel

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
    }

    @After
    fun tearDown() {
        unmockkAll()
    }

    @Test
    fun checkSettingsScreen() {
        // Mock datastore response
        coEvery { settingsViewModel.settingsPrefs } returns SettingsPrefs()

        composeTestRule.setContent {
            MyTheme {
                Surface {
                    SettingsBody(viewModel = settingsViewModel)
                }
            }
        }

        // Check how many items are in the list
        composeTestRule.onNodeWithText("Save").assertIsDisplayed()
    }

    @Test
    fun checkButtonClickInvalidEmail() {
        // Mock datastore response
        coEvery { settingsViewModel.settingsPrefs } returns SettingsPrefs()

        composeTestRule.setContent {
            MyTheme {
                Surface {
                    SettingsBody(viewModel = settingsViewModel)
                }
            }
        }

        // Perform
        composeTestRule.onNodeWithText("Save").performClick()
        // Check if the toast is displayed
        onView(withText(R.string.settings_invalid_email)).inRoot(ToastMatcher())
            .check(matches(isDisplayed()))
    }
}
