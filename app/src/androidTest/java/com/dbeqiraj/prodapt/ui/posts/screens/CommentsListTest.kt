package com.dbeqiraj.prodapt.ui.posts.screens

import androidx.compose.material.Surface
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.test.assertCountEquals
import androidx.compose.ui.test.junit4.createAndroidComposeRule
import androidx.compose.ui.test.onAllNodesWithText
import androidx.paging.ExperimentalPagingApi
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.dbeqiraj.domain.models.comments.Comment
import com.dbeqiraj.prodapt.MainActivity
import com.dbeqiraj.prodapt.theme.MyTheme
import com.dbeqiraj.prodapt.ui.posts.ui.screens.details.CommentsList
import com.google.accompanist.pager.ExperimentalPagerApi
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import kotlin.time.ExperimentalTime

@ExperimentalPagerApi
@ExperimentalComposeUiApi
@ExperimentalPagingApi
@ExperimentalCoroutinesApi
@ExperimentalTime
@RunWith(AndroidJUnit4::class)
class CommentsListTest {

    @get:Rule
    val composeTestRule = createAndroidComposeRule<MainActivity>()

    @Test
    fun checkCommentsList() {
        composeTestRule.setContent {
            MyTheme {
                Surface {
                    CommentsList(listOf(Comment.mock()))
                }
            }
        }
        // Check how many items are in the list
        composeTestRule.onAllNodesWithText(text = "comment", substring = true).assertCountEquals(1)
    }
}
