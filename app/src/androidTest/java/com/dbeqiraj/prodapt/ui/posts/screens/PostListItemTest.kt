package com.dbeqiraj.prodapt.ui.posts.screens

import androidx.compose.material.Surface
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.test.assertIsDisplayed
import androidx.compose.ui.test.junit4.createAndroidComposeRule
import androidx.compose.ui.test.onNodeWithText
import androidx.paging.ExperimentalPagingApi
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.dbeqiraj.domain.models.posts.Post
import com.dbeqiraj.prodapt.MainActivity
import com.dbeqiraj.prodapt.theme.MyTheme
import com.dbeqiraj.prodapt.ui.posts.ui.screens.list.HomePostItem
import com.google.accompanist.pager.ExperimentalPagerApi
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import kotlin.time.ExperimentalTime

@ExperimentalPagerApi
@ExperimentalComposeUiApi
@ExperimentalPagingApi
@ExperimentalCoroutinesApi
@ExperimentalTime
@RunWith(AndroidJUnit4::class)
class PostListItemTest {

    @get:Rule
    val composeTestRule = createAndroidComposeRule<MainActivity>()

    @Test
    fun checkPostListItem() {
        composeTestRule.setContent {
            MyTheme {
                Surface {
                    HomePostItem(Post.mock())
                }
            }
        }
        composeTestRule.onNodeWithText("Post body").assertIsDisplayed()
    }
}
