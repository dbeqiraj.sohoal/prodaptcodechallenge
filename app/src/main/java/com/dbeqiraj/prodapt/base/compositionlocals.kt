/*
 *     Created by Dionis Beqiraj on 10/08/2022, 16:42
 *     dbeqiraj.sohoal@gmail.com
 *     Last modified 10/08/2022, 16:41
 *     Copyright (c) 2022.
 *     All rights reserved.
 */

package com.dbeqiraj.prodapt.base

import androidx.compose.runtime.staticCompositionLocalOf
import com.dbeqiraj.prodapt.ui.common.ui.viewmodels.MainViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi

@OptIn(ExperimentalCoroutinesApi::class)
val LocalBaseViewModel =
    staticCompositionLocalOf<MainViewModel> { error("No MainViewModel found!") }
