/*
 *     Created by Dionis Beqiraj on 10/08/2022, 16:42
 *     dbeqiraj.sohoal@gmail.com
 *     Last modified 10/08/2022, 16:41
 *     Copyright (c) 2022.
 *     All rights reserved.
 */

package com.dbeqiraj.prodapt.routing

interface NavigationRoute {
    val route: String
}

interface NavigationRouteArgument1 : NavigationRoute {
    val argument0: String
    fun routeWithArguments(argument0: String?) = route
        .replace("{${this.argument0}}", argument0 ?: "")
}

interface NavigationRouteArgument2 : NavigationRouteArgument1 {
    val argument1: String
    fun routeWithArguments(
        argument0: String?,
        argument1: String?
    ) = routeWithArguments(argument0)
        .replace("{${this.argument1}}", argument1 ?: "")
}

interface NavigationRouteArgument3 : NavigationRouteArgument2 {
    val argument2: String
    fun routeWithArguments(
        argument0: String?,
        argument1: String?,
        argument2: String?
    ) = routeWithArguments(argument0, argument1)
        .replace("{${this.argument2}}", argument2 ?: "")
}

interface NavigationRouteArgument4 : NavigationRouteArgument3 {
    val argument3: String
    fun routeWithArguments(
        argument0: String?,
        argument1: String?,
        argument2: String?,
        argument3: String?
    ) = routeWithArguments(argument0, argument1, argument2)
        .replace("{${this.argument3}}", argument3 ?: "")
}

interface NavigationRouteArgument5 : NavigationRouteArgument4 {
    val argument4: String
    fun routeWithArguments(
        argument0: String?,
        argument1: String?,
        argument2: String?,
        argument3: String?,
        argument4: String?
    ) = routeWithArguments(argument0, argument1, argument2, argument3)
        .replace("{${this.argument4}}", argument4 ?: "")
}
