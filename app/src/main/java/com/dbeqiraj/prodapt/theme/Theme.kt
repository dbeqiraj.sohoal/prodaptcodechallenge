/*
 *     Created by Dionis Beqiraj on 10/08/2022, 16:42
 *     dbeqiraj.sohoal@gmail.com
 *     Last modified 10/08/2022, 16:41
 *     Copyright (c) 2022.
 *     All rights reserved.
 */

package com.dbeqiraj.prodapt.theme

import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.material.Colors
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable

// system palette colors
val DarkColorPalette: @Composable () -> Colors by lazy {
    { parseConfigPalette(false) }
}

val LightColorPalette: @Composable () -> Colors by lazy {
    { parseConfigPalette(true) }
}

@Composable
fun MyTheme(content: @Composable () -> Unit) {
    val colors = if (isSystemInDarkTheme()) {
        DarkColorPalette()
    } else {
        LightColorPalette()
    }

    MaterialTheme(
        colors = colors,
        typography = Typography,
        shapes = Shapes,
        content = content
    )
}

// custom palette colors
val DarkColorCustomPalette: @Composable () -> CustomColors by lazy {
    { parseConfigCustomPalette(false) }
}

val LightColorCustomPalette: @Composable () -> CustomColors by lazy {
    { parseConfigCustomPalette(true) }
}

object MaterialThemeCustom {
    val colors: CustomColors
        @Composable
        get() {
            return if (isSystemInDarkTheme()) {
                DarkColorCustomPalette()
            } else {
                LightColorCustomPalette()
            }
        }
}
