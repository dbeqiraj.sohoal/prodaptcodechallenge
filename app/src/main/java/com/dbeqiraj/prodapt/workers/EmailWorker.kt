package com.dbeqiraj.prodapt.workers

import android.content.Context
import android.text.format.DateFormat
import androidx.datastore.core.DataStore
import androidx.hilt.work.HiltWorker
import androidx.work.CoroutineWorker
import androidx.work.WorkerParameters
import com.dbeqiraj.data.utils.CoroutineDispatcherProvider
import com.dbeqiraj.datastore.prefs.settings.SettingsPrefs
import com.dbeqiraj.prodapt.BuildConfig
import com.dbeqiraj.prodapt.utils.extensions.isValidEmail
import com.dbeqiraj.prodaptkit.helpers.MailServiceProvider
import com.dbeqiraj.prodaptkit.prodaptanalytics.ProdaptAnalytics
import dagger.assisted.Assisted
import dagger.assisted.AssistedInject
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.withContext
import timber.log.Timber
import java.util.*

@HiltWorker
class EmailWorker @AssistedInject constructor(
    @Assisted private val context: Context,
    @Assisted private val workerParams: WorkerParameters,
    private val prodaptAnalytics: ProdaptAnalytics,
    private val datastore: DataStore<SettingsPrefs>,
    private val dispatcherProvider: CoroutineDispatcherProvider
) : CoroutineWorker(context, workerParams) {

    override suspend fun doWork(): Result = withContext(dispatcherProvider.io) {
        return@withContext try {
            // The email sent in the settings
            val sendTo = datastore.data.first().email
            // The list of events to be sent
            val events = prodaptAnalytics.getEvents()

            if (sendTo.isValidEmail() && events.isNotEmpty()) {
                val email = MailServiceProvider.Email(
                    auth = MailServiceProvider.UserPassAuthenticator(
                        username = USERNAME,
                        password = PASSWORD
                    ),
                    toList = listOf(sendTo),
                    from = USERNAME,
                    subject = getSubject(),
                    body = prodaptAnalytics.getPrettyEvents()
                )

                MailServiceProvider().send(email)
                // Clear the events pool (reset every session)
                prodaptAnalytics.clearEvents()
            } else {
                Timber.tag(TAG).d("The email set in the settings is not valid!")
            }
            Result.success()
        } catch (ex: Exception) {
            Timber.tag(TAG).e(ex)
            Result.failure()
        }
    }

    private fun getSubject(): String {
        val calendar: Calendar = Calendar.getInstance()
        return "Prodapt Analytics Report - ${DateFormat.format("dd-MM-yyyy HH:mm:ss", calendar)}"
    }

    companion object {
        private const val TAG = "EmailWorker"

        private const val USERNAME = BuildConfig.EMAIL_SERVICE_USERNAME
        private const val PASSWORD = BuildConfig.EMAIL_SERVICE_PASSWORD
    }
}
