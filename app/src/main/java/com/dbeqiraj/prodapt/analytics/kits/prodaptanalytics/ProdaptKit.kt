package com.dbeqiraj.prodapt.analytics.kits.prodaptanalytics

import com.dbeqiraj.prodaptkit.analyticsdispatcher.AnalyticsKit

class ProdaptKit private constructor() : AnalyticsKit {
    override val name: String
        get() = "ProdaptKit"

    private object Holder {
        val INSTANCE = ProdaptKit()
    }

    companion object {
        val instance: ProdaptKit by lazy { Holder.INSTANCE }
    }
}
