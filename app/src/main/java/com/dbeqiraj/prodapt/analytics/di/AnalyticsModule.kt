package com.dbeqiraj.prodapt.analytics.di

import android.content.Context
import com.dbeqiraj.prodapt.analytics.kits.AndroidAnalyticsSettings
import com.dbeqiraj.prodapt.analytics.kits.prodaptanalytics.ProdaptDispatcherImpl
import com.dbeqiraj.prodaptkit.analyticsdispatcher.Analytics
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import timber.log.Timber
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AnalyticsModule {

    @Singleton
    @Provides
    internal fun provideAnalytics(@ApplicationContext context: Context): Analytics {
        // set an analytics enabled / disabled via SharedPrefs, Database, or anything else
        val settings = AndroidAnalyticsSettings(context).also {
            it.isAnalyticsEnabled = true
        }

        // Init analytics property. this is in charge of tracking all events
        val analytics = Analytics(
            settings,
            ProdaptDispatcherImpl(init = true, context = context)
        ).also {
            // Set an exception handler. Either way, the analytics util won't crash your app
            it.exceptionHandler = object : Analytics.ExceptionHandler {
                override fun onException(e: Exception) {
                    // This is the exception, log it, send it or ignore it.
                    Timber.tag("Analytics").w("Analytics Exception Raised")
                    // Print exception in console
                    e.printStackTrace()
                }
            }
        }
        return analytics
    }
}
