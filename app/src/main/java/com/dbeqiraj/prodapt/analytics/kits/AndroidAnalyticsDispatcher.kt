package com.dbeqiraj.prodapt.analytics.kits

import android.content.Context
import com.dbeqiraj.prodaptkit.analyticsdispatcher.AnalyticsDispatcher

interface AndroidAnalyticsDispatcher : AnalyticsDispatcher {

    val context: Context
}
