package com.dbeqiraj.prodapt.analytics.kits

import android.content.Context
import com.dbeqiraj.prodaptkit.analyticsdispatcher.AnalyticsSettings

class AndroidAnalyticsSettings(val context: Context) : AnalyticsSettings()
