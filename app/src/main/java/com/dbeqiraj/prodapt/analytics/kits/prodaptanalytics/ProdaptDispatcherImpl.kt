package com.dbeqiraj.prodapt.analytics.kits.prodaptanalytics

import android.content.Context
import android.location.Location
import com.dbeqiraj.prodapt.analytics.events.base.ProdaptEvent.Companion.PARAM_ACTION
import com.dbeqiraj.prodapt.analytics.events.base.ProdaptEvent.Companion.PARAM_APP_ID
import com.dbeqiraj.prodapt.analytics.events.base.ProdaptEvent.Companion.PARAM_LOCATION
import com.dbeqiraj.prodapt.analytics.events.base.ProdaptEvent.Companion.PARAM_RESOURCE_ID
import com.dbeqiraj.prodapt.analytics.events.base.ProdaptEvent.Companion.PARAM_TIMESTAMP
import com.dbeqiraj.prodapt.analytics.events.base.ProdaptEvent.Companion.PARAM_USER_ID
import com.dbeqiraj.prodapt.analytics.kits.AndroidAnalyticsDispatcher
import com.dbeqiraj.prodaptkit.analyticsdispatcher.events.CustomEvent
import com.dbeqiraj.prodaptkit.prodaptanalytics.ProdaptAnalytics
import com.dbeqiraj.prodaptkit.prodaptanalytics.ProdaptAnalyticsEvent

class ProdaptDispatcherImpl(override val init: Boolean, override val context: Context) :
    AndroidAnalyticsDispatcher {

    //region - Properties

    private var prodaptAnalytics: ProdaptAnalytics? = null

    //endregion

    //region - Constructor

    constructor(context: Context) : this(true, context)

    //endregion

    //region - LifeCycle

    override val dispatcherName: String = DispatcherName

    override val kit = ProdaptKit.instance

    override fun initDispatcher() {
        prodaptAnalytics = ProdaptAnalytics
    }

    //endregion

    //region - Implement events tracking

    override fun trackCustomEvent(event: CustomEvent) {
        prodaptAnalytics?.logEvent(event.toProdaptEvent())
    }

    //endregion

    //region - Private Methods

    private fun CustomEvent.toProdaptEvent(): ProdaptAnalyticsEvent {
        val params = getParameters(kit)

        return ProdaptAnalyticsEvent(
            appId = "${params[PARAM_APP_ID]}",
            action = "${params[PARAM_ACTION]}",
            resourceId = "${params[PARAM_RESOURCE_ID]}",
            userId = "${params[PARAM_USER_ID]}",
            meta = ProdaptAnalyticsEvent.Meta(
                timestamp = params[PARAM_TIMESTAMP] as Long,
                location = ProdaptAnalyticsEvent.Meta.Location(
                    lat = "${(params[PARAM_LOCATION] as? Location)?.latitude}",
                    long = "${(params[PARAM_LOCATION] as? Location)?.longitude}"
                )
            )
        )
    }

    //endregion

    companion object {
        const val DispatcherName = "DefaultProdaptDispatcher"
    }
}
