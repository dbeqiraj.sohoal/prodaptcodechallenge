package com.dbeqiraj.prodapt.analytics.events

import com.dbeqiraj.prodapt.analytics.events.base.ProdaptEvent
import com.dbeqiraj.prodaptkit.analyticsdispatcher.AnalyticsKit
import com.dbeqiraj.prodaptkit.helpers.DeviceHelper
import com.dbeqiraj.prodaptkit.location.LocationDelegate

class PostDetailsEvent(
    private val deviceHelper: DeviceHelper,
    private val locationDelegate: LocationDelegate,
    private val postId: Int
) : ProdaptEvent(deviceHelper, locationDelegate) {

    override fun getParameters(kit: AnalyticsKit): MutableMap<String, Any?> {
        val parameters = super.getParameters(kit)

        parameters[PARAM_ACTION] = "view"
        parameters[PARAM_RESOURCE_ID] = postId

        return parameters
    }
}
