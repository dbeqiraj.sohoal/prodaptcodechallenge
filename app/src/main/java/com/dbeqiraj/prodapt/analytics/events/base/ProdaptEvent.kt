package com.dbeqiraj.prodapt.analytics.events.base

import com.dbeqiraj.prodapt.analytics.kits.prodaptanalytics.ProdaptKit
import com.dbeqiraj.prodaptkit.analyticsdispatcher.AnalyticsKit
import com.dbeqiraj.prodaptkit.analyticsdispatcher.events.CustomEvent
import com.dbeqiraj.prodaptkit.helpers.DeviceHelper
import com.dbeqiraj.prodaptkit.location.LocationDelegate

abstract class ProdaptEvent(
    private val deviceHelper: DeviceHelper,
    private val locationDelegate: LocationDelegate
) : CustomEvent {

    override val includedKits: List<AnalyticsKit>
        get() = listOf(ProdaptKit.instance)

    override fun getEventName(kit: AnalyticsKit): String = "prodapt_event"

    override fun getParameters(kit: AnalyticsKit): MutableMap<String, Any?> {
        val parameters = super.getParameters(kit)

        // Setting here the common params for ProdaptEvent

        parameters[PARAM_APP_ID] = "dbeqiraj.sohoal@gmail.com"
        parameters[PARAM_USER_ID] = deviceHelper.getDeviceId()
        parameters[PARAM_TIMESTAMP] = System.currentTimeMillis()
        parameters[PARAM_LOCATION] = locationDelegate.getLastKnownLocation()

        return parameters
    }

    companion object {
        internal const val PARAM_APP_ID = "appId"
        internal const val PARAM_USER_ID = "userId"
        internal const val PARAM_TIMESTAMP = "timestamp"
        internal const val PARAM_LOCATION = "location"
        internal const val PARAM_ACTION = "action"
        internal const val PARAM_RESOURCE_ID = "resourceId"
    }
}
