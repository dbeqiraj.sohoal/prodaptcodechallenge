/*
 *     Created by Dionis Beqiraj on 10/08/2022, 16:42
 *     dbeqiraj.sohoal@gmail.com
 *     Last modified 10/08/2022, 16:41
 *     Copyright (c) 2022.
 *     All rights reserved.
 */

package com.dbeqiraj.prodapt.utils.extensions

import android.text.TextUtils
import android.util.Patterns
import androidx.compose.ui.graphics.Color

fun String.toColor(): Color {
    return Color(
        when {
            this.contains("0xFF") -> android.graphics.Color.parseColor("#" + this.removePrefix("0xFF"))
            this.contains("#") -> android.graphics.Color.parseColor(this)
            else -> android.graphics.Color.parseColor("#$this")
        }
    )
}

fun String.isValidEmail() =
    !TextUtils.isEmpty(this) && Patterns.EMAIL_ADDRESS.matcher(this).matches()
