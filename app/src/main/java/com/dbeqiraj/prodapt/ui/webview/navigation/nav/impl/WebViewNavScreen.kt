package com.dbeqiraj.prodapt.ui.webview.navigation.nav.impl

import com.dbeqiraj.prodapt.routing.NavigationRouteArgument1

object WebViewNavScreen {

    val WebViewScreen = object : NavigationRouteArgument1 {
        override val argument0: String = "url"
        override val route: String = "WebViewScreen/{url}"
    }
}
