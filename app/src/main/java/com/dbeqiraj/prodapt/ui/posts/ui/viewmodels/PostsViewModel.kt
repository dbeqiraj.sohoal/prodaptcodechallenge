/*
 *     Created by Dionis Beqiraj on 10/08/2022, 16:42
 *     dbeqiraj.sohoal@gmail.com
 *     Last modified 10/08/2022, 16:41
 *     Copyright (c) 2022.
 *     All rights reserved.
 */

package com.dbeqiraj.prodapt.ui.posts.ui.viewmodels

import androidx.lifecycle.ViewModel
import androidx.paging.ExperimentalPagingApi
import androidx.paging.PagingData
import com.dbeqiraj.domain.models.posts.Post
import com.dbeqiraj.domain.repositories.PostsRepository
import com.dbeqiraj.prodapt.analytics.events.PaginateEvent
import com.dbeqiraj.prodapt.analytics.events.PostDetailsEvent
import com.dbeqiraj.prodapt.analytics.events.PostsScrollEvent
import com.dbeqiraj.prodaptkit.analyticsdispatcher.Analytics
import com.dbeqiraj.prodaptkit.helpers.DeviceHelper
import com.dbeqiraj.prodaptkit.location.LocationDelegate
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.distinctUntilChanged
import javax.inject.Inject

@HiltViewModel
class PostsViewModel @Inject constructor(
    private val postsRepository: PostsRepository,
    private val analytics: Analytics,
    private val deviceHelper: DeviceHelper,
    private val locationDelegate: LocationDelegate
) : ViewModel() {

    // region - flows

    @ExperimentalPagingApi
    val listPosts: Flow<PagingData<Post>> = postsRepository.getPosts()

    // endregion

    // region - Public funs

    fun getPostById(id: Int): Flow<Post> = postsRepository.getPostById(id).distinctUntilChanged()

    // endregion

    // region - Analytics log events

    fun logPaginateEvent(postId: Int) {
        analytics.track(PaginateEvent(deviceHelper, locationDelegate, postId))
    }

    fun logPostDetailsEvent(postId: Int) {
        analytics.track(PostDetailsEvent(deviceHelper, locationDelegate, postId))
    }

    fun logPostsScrollEvent(postId: Int) {
        analytics.track(PostsScrollEvent(deviceHelper, locationDelegate, postId))
    }

    // endregion
}
