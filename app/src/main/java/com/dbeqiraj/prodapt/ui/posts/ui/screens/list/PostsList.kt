/*
 *     Created by Dionis Beqiraj on 10/08/2022, 16:42
 *     dbeqiraj.sohoal@gmail.com
 *     Last modified 10/08/2022, 16:41
 *     Copyright (c) 2022.
 *     All rights reserved.
 */

package com.dbeqiraj.prodapt.ui.posts.ui.screens.list

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.paging.LoadState
import androidx.paging.compose.LazyPagingItems
import com.dbeqiraj.domain.models.posts.Post
import com.dbeqiraj.prodapt.R
import com.dbeqiraj.prodapt.ui.posts.ui.events.PostsListEvents
import com.dbeqiraj.prodapt.utils.compose.SwipeRefreshList
import com.google.accompanist.pager.ExperimentalPagerApi
import com.google.accompanist.swiperefresh.rememberSwipeRefreshState
import kotlinx.coroutines.ExperimentalCoroutinesApi

@ExperimentalCoroutinesApi
@ExperimentalPagerApi
@Composable
fun PostsList(
    onEvent: (PostsListEvents) -> Unit = {},
    posts: LazyPagingItems<Post>
) {
    SwipeRefreshList(
        items = posts,
        key = { _, post -> post.id },
        refreshState = rememberSwipeRefreshState(posts.loadState.refresh is LoadState.Loading),
        contentEmpty = {
            Text(
                stringResource(id = R.string.no_content_text),
                modifier = Modifier
                    .fillMaxSize()
                    .background(Color.White)
            )
        },
        contentLoadState = {
            if (it is LoadState.Loading) {
                Text(
                    stringResource(id = R.string.is_loading_text),
                    modifier = Modifier
                        .fillMaxSize()
                        .background(Color.White)
                )
            }
        },
        onNewPage = { startIndex ->
            posts[startIndex]?.let {
                // Fire paginate event
                onEvent(PostsListEvents.OnNewPage(it.id))
            }
        },
        onScrollFinished = { lastVisibleIndex ->
            posts[lastVisibleIndex]?.let {
                // Fire scroll event (send last visible post)
                onEvent(PostsListEvents.OnScrollFinished(it.id))
            }
        }
    ) { _, post ->
        HomePostItem(post = post, onEvent = onEvent)
    }
}
