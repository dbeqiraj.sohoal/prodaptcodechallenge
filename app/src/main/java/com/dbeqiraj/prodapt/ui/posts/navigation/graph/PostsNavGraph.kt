/*
 *     Created by Dionis Beqiraj on 10/08/2022, 16:42
 *     dbeqiraj.sohoal@gmail.com
 *     Last modified 10/08/2022, 16:41
 *     Copyright (c) 2022.
 *     All rights reserved.
 */

package com.dbeqiraj.prodapt.ui.posts.navigation.graph

import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.navigation.NavGraphBuilder
import androidx.paging.ExperimentalPagingApi
import com.dbeqiraj.prodapt.ui.common.navigation.NavActions
import com.dbeqiraj.prodapt.ui.posts.navigation.graph.impl.postsScreenGraph
import com.google.accompanist.pager.ExperimentalPagerApi

@ExperimentalPagingApi
@ExperimentalPagerApi
@ExperimentalComposeUiApi
fun NavGraphBuilder.postsNavGraph(
    navActions: NavActions
) {
    postsScreenGraph(navActions)
}
