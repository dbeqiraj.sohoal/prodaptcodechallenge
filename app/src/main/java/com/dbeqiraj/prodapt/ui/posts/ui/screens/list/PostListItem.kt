/*
 *     Created by Dionis Beqiraj on 10/08/2022, 16:42
 *     dbeqiraj.sohoal@gmail.com
 *     Last modified 10/08/2022, 16:41
 *     Copyright (c) 2022.
 *     All rights reserved.
 */

package com.dbeqiraj.prodapt.ui.posts.ui.screens.list

import android.content.res.Configuration
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.Card
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import coil.compose.AsyncImage
import com.dbeqiraj.domain.models.posts.Post
import com.dbeqiraj.prodapt.R
import com.dbeqiraj.prodapt.theme.MaterialThemeCustom
import com.dbeqiraj.prodapt.theme.MyTheme
import com.dbeqiraj.prodapt.ui.posts.ui.events.PostsListEvents
import com.google.accompanist.pager.ExperimentalPagerApi
import kotlinx.coroutines.ExperimentalCoroutinesApi

@OptIn(ExperimentalMaterialApi::class)
@ExperimentalCoroutinesApi
@ExperimentalPagerApi
@Composable
fun HomePostItem(
    post: Post,
    onEvent: (PostsListEvents) -> Unit = {}
) {
    val context = LocalContext.current
    Card(
        modifier = Modifier
            .padding(8.dp)
            .fillMaxWidth(),
        elevation = 5.dp,
        onClick = {
            onEvent(PostsListEvents.OnPostClick(post.id))
        }
    ) {
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .background(colorResource(id = R.color.primary))
        ) {
            AsyncImage(
                modifier = Modifier
                    .fillMaxWidth()
                    .aspectRatio(16 / 9f),
                contentScale = ContentScale.FillWidth,
                model = post.imageUrl,
                contentDescription = null
            )

            Column(modifier = Modifier.padding(8.dp)) {
                post.user?.name?.let {
                    Text(
                        text = it,
                        color = MaterialThemeCustom.colors.cardUser,
                        fontWeight = FontWeight.Bold
                    )
                }
                Text(
                    text = post.title,
                    color = MaterialThemeCustom.colors.cardTitle,
                    fontWeight = FontWeight.SemiBold
                )
                Text(text = post.body, color = MaterialThemeCustom.colors.cardBody)
                post.comments?.let {
                    Spacer(modifier = Modifier.height(10.dp))
                    Text(
                        text = context.resources.getQuantityString(
                            R.plurals.comment_text,
                            it.size,
                            it.size
                        ),
                        color = MaterialThemeCustom.colors.cardComments
                    )
                }
            }
        }
    }
}

@OptIn(ExperimentalPagerApi::class, ExperimentalCoroutinesApi::class)
@ExperimentalComposeUiApi
@Preview("Light")
@Preview("Dark", uiMode = Configuration.UI_MODE_NIGHT_YES)
@Composable
fun HomePostItemPreview() {
    MyTheme {
        Surface {
            HomePostItem(Post.mock())
        }
    }
}
