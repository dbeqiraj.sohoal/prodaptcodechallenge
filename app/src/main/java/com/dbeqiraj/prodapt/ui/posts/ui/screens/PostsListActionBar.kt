package com.dbeqiraj.prodapt.ui.posts.ui.screens

import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Settings
import androidx.compose.runtime.Composable
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.paging.ExperimentalPagingApi
import com.dbeqiraj.prodapt.R
import com.dbeqiraj.prodapt.theme.MaterialThemeCustom
import com.dbeqiraj.prodapt.ui.posts.ui.events.PostsListEvents
import com.google.accompanist.pager.ExperimentalPagerApi
import kotlinx.coroutines.ExperimentalCoroutinesApi

@ExperimentalCoroutinesApi
@ExperimentalPagingApi
@ExperimentalPagerApi
@ExperimentalComposeUiApi
@Composable
fun PostsListActionBar(modifier: Modifier = Modifier, onEvent: (PostsListEvents) -> Unit = {}) {
    TopAppBar(
        modifier = modifier,
        title = {
            Text(
                stringResource(id = R.string.app_name_display_text),
                color = MaterialThemeCustom.colors.title
            )
        },
        backgroundColor = MaterialTheme.colors.primaryVariant,
        actions = {
            // Action bar icon - Settings
            IconButton(onClick = {
                onEvent(PostsListEvents.OnSettings)
            }) {
                Icon(
                    Icons.Default.Settings,
                    stringResource(id = R.string.settings_text),
                    tint = MaterialThemeCustom.colors.iconTint
                )
            }
        }
    )
}
