package com.dbeqiraj.prodapt.ui.posts.ui.screens.details

import android.annotation.SuppressLint
import android.content.res.Configuration
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.Divider
import androidx.compose.material.Icon
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.KeyboardArrowDown
import androidx.compose.material.icons.filled.KeyboardArrowUp
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.dbeqiraj.domain.models.comments.Comment
import com.dbeqiraj.prodapt.R
import com.dbeqiraj.prodapt.theme.MaterialThemeCustom
import com.dbeqiraj.prodapt.theme.MyTheme
import com.google.accompanist.pager.ExperimentalPagerApi
import kotlinx.coroutines.ExperimentalCoroutinesApi

@SuppressLint("UnrememberedMutableState")
@OptIn(ExperimentalPagerApi::class, ExperimentalCoroutinesApi::class)
@Composable
fun CommentsList(
    comments: List<Comment>
) {
    val context = LocalContext.current
    var collapsed by remember { mutableStateOf(true) }
    Column {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .clickable {
                    collapsed = !collapsed
                },
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.Center
        ) {
            Icon(
                Icons.Default.run {
                    if (collapsed) {
                        KeyboardArrowDown
                    } else {
                        KeyboardArrowUp
                    }
                },
                contentDescription = "",
                tint = MaterialThemeCustom.colors.iconTint
            )
            Text(
                text = context.resources.getQuantityString(
                    R.plurals.comment_text,
                    comments.size,
                    comments.size
                ),
                fontWeight = FontWeight.Bold,
                modifier = Modifier
                    .padding(vertical = 10.dp)
            )
        }
        Divider()
        if (!collapsed) {
            comments.forEach { comment ->
                Row {
                    Spacer(modifier = Modifier.size(5.dp))
                    CommentItem(comment = comment)
                }
                Divider()
            }
        }
    }
}

@ExperimentalComposeUiApi
@Preview("Light")
@Preview("Dark", uiMode = Configuration.UI_MODE_NIGHT_YES)
@Composable
fun CommentsListPreview() {
    MyTheme {
        Surface {
            CommentsList(listOf(Comment.mock()))
        }
    }
}
