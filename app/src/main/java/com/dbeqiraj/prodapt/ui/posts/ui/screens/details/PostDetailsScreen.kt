package com.dbeqiraj.prodapt.ui.posts.ui.screens.details

import android.content.res.Configuration
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Email
import androidx.compose.material.icons.filled.Person
import androidx.compose.material.icons.filled.Phone
import androidx.compose.material.icons.filled.Web
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import coil.compose.AsyncImage
import com.dbeqiraj.domain.models.posts.Post
import com.dbeqiraj.prodapt.R
import com.dbeqiraj.prodapt.theme.MaterialThemeCustom
import com.dbeqiraj.prodapt.theme.MyTheme
import com.dbeqiraj.prodapt.ui.common.ui.screens.ErrorScreen
import com.dbeqiraj.prodapt.ui.posts.ui.events.PostsListEvents
import com.dbeqiraj.prodapt.ui.posts.ui.viewmodels.PostsViewModel
import com.dbeqiraj.prodapt.utils.extensions.composeEmail
import com.dbeqiraj.prodapt.utils.extensions.dialNumber
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.maps.android.compose.GoogleMap
import com.google.maps.android.compose.Marker
import com.google.maps.android.compose.MarkerState
import com.google.maps.android.compose.rememberCameraPositionState
import kotlinx.coroutines.ExperimentalCoroutinesApi
import java.net.URLEncoder
import java.nio.charset.StandardCharsets

@OptIn(ExperimentalMaterialApi::class)
@ExperimentalCoroutinesApi
@Composable
fun PostDetailsScreen(
    postId: Int?,
    viewModel: PostsViewModel,
    onEvent: (PostsListEvents) -> Unit = {}
) {
    postId?.let {
        val postFlow = viewModel.getPostById(it).collectAsState(initial = null)
        postFlow.value?.let { post ->
            PostDetails(post = post, onEvent = onEvent)
        } ?: run {
            // Error screen
            ErrorScreen(stringResource(id = R.string.post_details_no_data))
        }
    } ?: run {
        // Error screen
        ErrorScreen(stringResource(id = R.string.post_details_no_data))
    }
}

@Composable
fun PostDetails(post: Post, onEvent: (PostsListEvents) -> Unit = {}) {
    val context = LocalContext.current
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .verticalScroll(rememberScrollState())
            .background(MaterialTheme.colors.primary)
    ) {
        AsyncImage(
            modifier = Modifier
                .fillMaxWidth()
                .aspectRatio(16 / 9f),
            contentScale = ContentScale.FillWidth,
            model = post.imageUrl,
            contentDescription = null
        )
        Column(modifier = Modifier.padding(8.dp)) {
            Text(
                text = post.title,
                color = MaterialThemeCustom.colors.cardTitle,
                fontWeight = FontWeight.SemiBold
            )
            Text(text = post.body, color = MaterialThemeCustom.colors.cardBody)

            Divider(
                modifier = Modifier.padding(vertical = 8.dp),
                color = colorResource(id = R.color.post_details_divider),
                thickness = 1.dp
            )

            // user
            post.user?.let {
                // Name
                Row(modifier = Modifier.padding(vertical = 5.dp)) {
                    Icon(imageVector = Icons.Filled.Person, contentDescription = null)
                    Text(
                        modifier = Modifier.padding(start = 8.dp),
                        text = it.name,
                        color = MaterialThemeCustom.colors.cardUser,
                        fontWeight = FontWeight.Bold
                    )
                }
                // E-mail
                Row(
                    modifier = Modifier
                        .padding(vertical = 5.dp)
                        .clickable {
                            context.composeEmail(arrayOf(it.email), "Hello ${it.name}")
                        }
                ) {
                    Icon(imageVector = Icons.Filled.Email, contentDescription = null)
                    Text(
                        modifier = Modifier.padding(start = 8.dp),
                        text = it.email,
                        color = colorResource(id = R.color.post_detail_web)
                    )
                }
                // Website
                Row(
                    modifier = Modifier
                        .padding(vertical = 5.dp)
                        .clickable {
                            // The url's from the user come incomplete, so append http in case it is missing
                            val safeUrl = if (!it.website.contains("http")) {
                                "http://${it.website}"
                            } else {
                                it.website
                            }
                            val encodedUrl =
                                URLEncoder.encode(safeUrl, StandardCharsets.UTF_8.toString())
                            // Go to webview screen
                            onEvent(PostsListEvents.OnOpenWebView(encodedUrl))
                        }
                ) {
                    Icon(imageVector = Icons.Filled.Web, contentDescription = null)
                    Text(
                        modifier = Modifier.padding(start = 8.dp),
                        text = it.website,
                        color = colorResource(id = R.color.post_detail_web)
                    )
                }
                // Phone
                Row(
                    modifier = Modifier
                        .padding(vertical = 5.dp)
                        .clickable {
                            context.dialNumber(phoneNumber = it.phone)
                        }
                ) {
                    Icon(imageVector = Icons.Filled.Phone, contentDescription = null)
                    Text(
                        modifier = Modifier.padding(start = 8.dp),
                        text = it.phone,
                        color = colorResource(id = R.color.post_detail_web)
                    )
                }
                // Map
                it.address.geo.run {
                    val location = LatLng(lat.toDouble(), lng.toDouble())
                    val cameraPositionState = rememberCameraPositionState {
                        position = CameraPosition.fromLatLngZoom(location, 2f)
                    }
                    GoogleMap(
                        modifier = Modifier
                            .fillMaxWidth()
                            .aspectRatio(16 / 9f),
                        cameraPositionState = cameraPositionState
                    ) {
                        Marker(
                            state = MarkerState(position = location),
                            title = it.address.city
                        )
                    }
                }
            }

            Divider(
                modifier = Modifier.padding(vertical = 8.dp),
                color = colorResource(id = R.color.post_details_divider),
                thickness = 1.dp
            )

            // Comments
            post.comments?.let { CommentsList(comments = it) }
        }
    }
}

@ExperimentalComposeUiApi
@Preview("Light")
@Preview("Dark", uiMode = Configuration.UI_MODE_NIGHT_YES)
@Composable
fun PostDetailsPreview() {
    MyTheme {
        Surface {
            PostDetails(Post.mock())
        }
    }
}
