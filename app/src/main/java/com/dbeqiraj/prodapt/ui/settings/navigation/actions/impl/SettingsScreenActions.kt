package com.dbeqiraj.prodapt.ui.settings.navigation.actions.impl

import androidx.navigation.NavHostController
import com.dbeqiraj.prodapt.ui.settings.navigation.nav.SettingsNav

interface SettingsScreenActions {

    val controller: NavHostController

    fun navigateToSettings() {
        controller.navigate(SettingsNav.MainNav.SettingsScreen.route)
    }
}
