package com.dbeqiraj.prodapt.ui.webview.navigation.graph

import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.navigation.NavGraphBuilder
import androidx.paging.ExperimentalPagingApi
import com.dbeqiraj.prodapt.ui.common.navigation.NavActions
import com.dbeqiraj.prodapt.ui.webview.navigation.graph.impl.webViewScreenGraph
import com.google.accompanist.pager.ExperimentalPagerApi

@ExperimentalPagingApi
@ExperimentalPagerApi
@ExperimentalComposeUiApi
fun NavGraphBuilder.webViewNavGraph(
    navActions: NavActions
) {
    webViewScreenGraph()
}
