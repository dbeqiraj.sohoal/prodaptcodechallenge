/*
 *     Created by Dionis Beqiraj on 10/08/2022, 16:42
 *     dbeqiraj.sohoal@gmail.com
 *     Last modified 10/08/2022, 16:41
 *     Copyright (c) 2022.
 *     All rights reserved.
 */

package com.dbeqiraj.prodapt.ui.common.navigation

import androidx.navigation.NavHostController
import com.dbeqiraj.prodapt.ui.posts.navigation.actions.PostsNavActions
import com.dbeqiraj.prodapt.ui.settings.navigation.actions.SettingsNavActions
import com.dbeqiraj.prodapt.ui.webview.navigation.actions.WevViewNavActions

class NavActions(
    override val controller: NavHostController
) : PostsNavActions,
    SettingsNavActions,
    WevViewNavActions {

    val navigateToUp: () -> Unit = {
        controller.navigateUp()
    }
}
