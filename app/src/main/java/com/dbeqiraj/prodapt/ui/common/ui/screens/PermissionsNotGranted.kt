package com.dbeqiraj.prodapt.ui.common.ui.screens

import androidx.compose.foundation.layout.*
import androidx.compose.material.Button
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import com.dbeqiraj.prodapt.R
import com.dbeqiraj.prodapt.theme.MaterialThemeCustom
import com.google.accompanist.insets.ProvideWindowInsets
import com.google.accompanist.permissions.ExperimentalPermissionsApi
import com.google.accompanist.permissions.MultiplePermissionsState

@OptIn(ExperimentalPermissionsApi::class)
@Composable
fun PermissionNotGranted(locationPermissionState: MultiplePermissionsState) {
    ProvideWindowInsets {
        Scaffold {
            Box(
                modifier = Modifier.padding(it)
            ) {
                Column(
                    modifier = Modifier
                        .fillMaxHeight()
                        .fillMaxWidth()
                        .padding(8.dp),
                    verticalArrangement = Arrangement.Center,
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    ErrorScreen(message = stringResource(id = R.string.settings_permissions_not_granted))
                    Button(
                        colors = MaterialThemeCustom.colors.buttonColors,
                        onClick = {
                            locationPermissionState.launchMultiplePermissionRequest()
                        }
                    ) {
                        Text(stringResource(id = R.string.settings_permissions_request))
                    }
                }
            }
        }
    }
}
