/*
 *     Created by Dionis Beqiraj on 10/08/2022, 16:42
 *     dbeqiraj.sohoal@gmail.com
 *     Last modified 10/08/2022, 16:41
 *     Copyright (c) 2022.
 *     All rights reserved.
 */

package com.dbeqiraj.prodapt.ui.common.navigation

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.paging.ExperimentalPagingApi
import com.dbeqiraj.prodapt.ui.posts.navigation.graph.postsNavGraph
import com.dbeqiraj.prodapt.ui.posts.navigation.nav.PostsNav
import com.dbeqiraj.prodapt.ui.settings.navigation.graph.settingsNavGraph
import com.dbeqiraj.prodapt.ui.webview.navigation.graph.webViewNavGraph
import com.dbeqiraj.prodapt.utils.extensions.AddChangeRouteListener
import com.google.accompanist.insets.ProvideWindowInsets
import com.google.accompanist.pager.ExperimentalPagerApi
import kotlinx.coroutines.ExperimentalCoroutinesApi

@ExperimentalPagingApi
@ExperimentalPagerApi
@ExperimentalCoroutinesApi
@ExperimentalComposeUiApi
@Composable
fun NavGraph(navController: NavHostController) {
    navController.AddChangeRouteListener()

    val navActions = remember(navController) {
        NavActions(navController)
    }

    ProvideWindowInsets {
        Scaffold {
            Box(
                modifier = Modifier.padding(it)
            ) {
                NavHost(
                    navController = navController,
                    startDestination = PostsNav.MainNav.PostsListScreen.route
                ) {
                    postsNavGraph(
                        navActions = navActions
                    )
                    settingsNavGraph(
                        navActions = navActions
                    )
                    webViewNavGraph(
                        navActions = navActions
                    )
                }
            }
        }
    }
}
