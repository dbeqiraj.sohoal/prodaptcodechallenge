/*
 *     Created by Dionis Beqiraj on 10/08/2022, 16:42
 *     dbeqiraj.sohoal@gmail.com
 *     Last modified 10/08/2022, 16:41
 *     Copyright (c) 2022.
 *     All rights reserved.
 */

package com.dbeqiraj.prodapt.ui.posts.navigation.actions.impl

import androidx.navigation.NavHostController
import com.dbeqiraj.prodapt.ui.posts.navigation.nav.PostsNav

interface PostsScreenActions {

    val controller: NavHostController

    fun navigateToHome() {
        controller.navigate(PostsNav.MainNav.PostsListScreen.route)
    }

    fun navigateToPostDetails(postId: String) {
        PostsNav.MainNav.PostDetailsScreen.apply {
            controller.navigate(routeWithArguments(postId))
        }
    }
}
