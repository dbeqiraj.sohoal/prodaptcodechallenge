/*
 *     Created by Dionis Beqiraj on 10/08/2022, 16:42
 *     dbeqiraj.sohoal@gmail.com
 *     Last modified 10/08/2022, 16:41
 *     Copyright (c) 2022.
 *     All rights reserved.
 */

package com.dbeqiraj.prodapt.ui.settings.ui.viewmodels

import androidx.datastore.core.DataStore
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.ExperimentalPagingApi
import com.dbeqiraj.datastore.prefs.settings.SettingsPrefs
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import javax.inject.Inject

@HiltViewModel
class SettingsViewModel @Inject constructor(
    private val datastore: DataStore<SettingsPrefs>
) : ViewModel() {

    // region - flows

    private val _saved: MutableStateFlow<Boolean> = MutableStateFlow(false)
    val saved: StateFlow<Boolean> get() = _saved.asStateFlow()

    // endregion

    @ExperimentalPagingApi
    val settingsPrefs: SettingsPrefs = runBlocking {
        datastore.data.first()
    }

    // endregion

    // region - Public Methods

    fun saveEmail(email: String) {
        viewModelScope.launch(Dispatchers.Main) {
            datastore.updateData {
                it.copy(email = email)
            }
            _saved.value = true
        }
    }

    // endregion
}
