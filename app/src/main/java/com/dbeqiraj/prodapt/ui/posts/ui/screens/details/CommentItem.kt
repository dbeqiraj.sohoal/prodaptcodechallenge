package com.dbeqiraj.prodapt.ui.posts.ui.screens.details

import android.content.res.Configuration
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.dbeqiraj.domain.models.comments.Comment
import com.dbeqiraj.prodapt.R
import com.dbeqiraj.prodapt.theme.MaterialThemeCustom
import com.dbeqiraj.prodapt.theme.MyTheme
import com.google.accompanist.pager.ExperimentalPagerApi
import kotlinx.coroutines.ExperimentalCoroutinesApi

@OptIn(ExperimentalMaterialApi::class, ExperimentalComposeUiApi::class)
@ExperimentalCoroutinesApi
@ExperimentalPagerApi
@Composable
fun CommentItem(comment: Comment) {
    Column(
        modifier = Modifier
            .padding(4.dp)
            .fillMaxWidth()
    ) {
        Text(
            text = comment.name,
            color = MaterialThemeCustom.colors.cardUser,
            fontWeight = FontWeight.Medium
        )
        Text(
            text = comment.email,
            color = colorResource(id = R.color.post_detail_web),
            fontWeight = FontWeight.Medium
        )
        Text(
            modifier = Modifier.padding(top = 8.dp),
            text = comment.body,
            color = MaterialThemeCustom.colors.cardBody
        )
    }
}

@OptIn(ExperimentalPagerApi::class, ExperimentalCoroutinesApi::class)
@ExperimentalComposeUiApi
@Preview("Light")
@Preview("Dark", uiMode = Configuration.UI_MODE_NIGHT_YES)
@Composable
fun CommentItemPreview() {
    MyTheme {
        Surface {
            CommentItem(Comment.mock())
        }
    }
}
