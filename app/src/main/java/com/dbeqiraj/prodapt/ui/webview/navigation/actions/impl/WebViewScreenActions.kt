package com.dbeqiraj.prodapt.ui.webview.navigation.actions.impl

import androidx.navigation.NavHostController
import com.dbeqiraj.prodapt.ui.webview.navigation.nav.WebViewNav

interface WebViewScreenActions {

    val controller: NavHostController

    fun navigateWebView(url: String) {
        WebViewNav.MainNav.WebViewScreen.apply {
            controller.navigate(routeWithArguments(url))
        }
    }
}
