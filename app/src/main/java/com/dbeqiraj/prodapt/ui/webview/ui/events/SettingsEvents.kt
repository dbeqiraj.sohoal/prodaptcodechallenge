package com.dbeqiraj.prodapt.ui.webview.ui.events

sealed class SettingsEvents {
    data class SaveEmail(val email: String) : SettingsEvents()
    object NavigateBack : SettingsEvents()
}
