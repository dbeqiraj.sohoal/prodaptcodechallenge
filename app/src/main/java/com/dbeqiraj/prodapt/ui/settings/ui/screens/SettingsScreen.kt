package com.dbeqiraj.prodapt.ui.settings.ui.screens

import android.content.Context
import android.widget.Toast
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Email
import androidx.compose.material.icons.filled.Save
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.unit.dp
import androidx.paging.ExperimentalPagingApi
import com.dbeqiraj.prodapt.R
import com.dbeqiraj.prodapt.theme.MaterialThemeCustom
import com.dbeqiraj.prodapt.ui.settings.ui.viewmodels.SettingsViewModel
import com.dbeqiraj.prodapt.ui.webview.ui.events.SettingsEvents
import com.dbeqiraj.prodapt.utils.extensions.isValidEmail

@Composable
fun SettingsScreen(
    viewModel: SettingsViewModel,
    onEvent: (SettingsEvents) -> Unit = {}
) {
    val saved: Boolean by viewModel.saved.collectAsState()

    if (saved) {
        onEvent(SettingsEvents.NavigateBack)
    } else {
        SettingsBody(viewModel = viewModel, onEvent = onEvent)
    }
}

@OptIn(ExperimentalPagingApi::class)
@Composable
fun SettingsBody(
    viewModel: SettingsViewModel,
    onEvent: (SettingsEvents) -> Unit = {}
) {
    val context = LocalContext.current
    val settingsPrefs = viewModel.settingsPrefs

    Column(
        modifier = Modifier
            .fillMaxHeight()
            .fillMaxWidth()
            .padding(8.dp),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        var text by remember { mutableStateOf(TextFieldValue(settingsPrefs.email)) }

        // The email text field
        OutlinedTextField(
            value = text,
            leadingIcon = {
                Icon(
                    imageVector = Icons.Default.Email,
                    contentDescription = null,
                    tint = MaterialThemeCustom.colors.iconTint
                )
            },
            onValueChange = {
                text = it
            },
            label = { Text(text = stringResource(id = R.string.settings_email_label)) },
            placeholder = { Text(text = stringResource(id = R.string.settings_email_placeholder)) },
            colors = MaterialThemeCustom.colors.textFieldColors
        )
        // Save button
        Button(
            modifier = Modifier
                .padding(top = 10.dp),
            contentPadding = PaddingValues(
                start = 20.dp,
                top = 12.dp,
                end = 20.dp,
                bottom = 12.dp
            ),
            onClick = {
                onSaveClick(onEvent = onEvent, context = context, email = text.text)
            },
            colors = MaterialThemeCustom.colors.buttonColors
        ) {
            Icon(
                Icons.Filled.Save,
                contentDescription = "Save",
                modifier = Modifier.size(ButtonDefaults.IconSize)
            )
            Spacer(Modifier.size(ButtonDefaults.IconSpacing))
            Text(stringResource(id = R.string.settings_save_text))
        }
    }
}

private fun onSaveClick(
    onEvent: (SettingsEvents) -> Unit,
    context: Context,
    email: String
) {
    if (email.isValidEmail()) {
        onEvent(SettingsEvents.SaveEmail(email = email))
    } else {
        Toast.makeText(
            context,
            context.getText(R.string.settings_invalid_email),
            Toast.LENGTH_SHORT
        ).show()
    }
}
