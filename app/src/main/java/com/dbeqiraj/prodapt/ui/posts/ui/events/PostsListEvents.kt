/*
 *     Created by Dionis Beqiraj on 10/08/2022, 16:42
 *     dbeqiraj.sohoal@gmail.com
 *     Last modified 10/08/2022, 16:41
 *     Copyright (c) 2022.
 *     All rights reserved.
 */

package com.dbeqiraj.prodapt.ui.posts.ui.events

sealed class PostsListEvents {
    object OnSettings : PostsListEvents()
    data class OnPostClick(val postId: Int) : PostsListEvents()
    data class OnOpenWebView(val url: String) : PostsListEvents()
    data class OnNewPage(val postId: Int) : PostsListEvents()
    data class OnScrollFinished(val lastVisiblePostId: Int) : PostsListEvents()
}
