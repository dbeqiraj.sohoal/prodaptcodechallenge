package com.dbeqiraj.prodapt.ui.settings.navigation.actions

import com.dbeqiraj.prodapt.ui.settings.navigation.actions.impl.SettingsScreenActions

interface SettingsNavActions : SettingsScreenActions
