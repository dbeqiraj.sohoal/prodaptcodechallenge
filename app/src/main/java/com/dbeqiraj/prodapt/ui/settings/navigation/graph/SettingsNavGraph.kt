package com.dbeqiraj.prodapt.ui.settings.navigation.graph

import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.navigation.NavGraphBuilder
import androidx.paging.ExperimentalPagingApi
import com.dbeqiraj.prodapt.ui.common.navigation.NavActions
import com.dbeqiraj.prodapt.ui.settings.navigation.graph.impl.settingsScreenGraph
import com.google.accompanist.pager.ExperimentalPagerApi

@ExperimentalPagingApi
@ExperimentalPagerApi
@ExperimentalComposeUiApi
fun NavGraphBuilder.settingsNavGraph(
    navActions: NavActions
) {
    settingsScreenGraph(navActions)
}
