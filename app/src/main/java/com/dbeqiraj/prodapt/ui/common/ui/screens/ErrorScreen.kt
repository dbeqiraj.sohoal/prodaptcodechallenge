package com.dbeqiraj.prodapt.ui.common.ui.screens

import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import com.dbeqiraj.prodapt.theme.MaterialThemeCustom

@Composable
fun ErrorScreen(message: String) {
    Text(
        text = message,
        color = MaterialThemeCustom.colors.cardTitle,
        textAlign = TextAlign.Center,
        fontWeight = FontWeight.ExtraBold
    )
}
