/*
 *     Created by Dionis Beqiraj on 10/08/2022, 16:42
 *     dbeqiraj.sohoal@gmail.com
 *     Last modified 10/08/2022, 16:41
 *     Copyright (c) 2022.
 *     All rights reserved.
 */

package com.dbeqiraj.prodapt.ui.posts.ui.screens

import androidx.compose.foundation.layout.Column
import androidx.compose.runtime.Composable
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.paging.ExperimentalPagingApi
import androidx.paging.compose.LazyPagingItems
import androidx.paging.compose.collectAsLazyPagingItems
import com.dbeqiraj.domain.models.posts.Post
import com.dbeqiraj.prodapt.ui.posts.ui.events.PostsListEvents
import com.dbeqiraj.prodapt.ui.posts.ui.screens.list.PostsList
import com.dbeqiraj.prodapt.ui.posts.ui.viewmodels.PostsViewModel
import com.google.accompanist.pager.ExperimentalPagerApi
import kotlinx.coroutines.ExperimentalCoroutinesApi

@ExperimentalCoroutinesApi
@ExperimentalPagingApi
@ExperimentalPagerApi
@ExperimentalComposeUiApi
@Composable
fun PostsListScreen(
    viewModel: PostsViewModel,
    onEvent: (PostsListEvents) -> Unit = {}
) {
    val posts: LazyPagingItems<Post> = viewModel.listPosts.collectAsLazyPagingItems()

    Column {
        // Action bar
        PostsListActionBar(onEvent = onEvent)
        // List of posts
        PostsList(posts = posts, onEvent = onEvent)
    }
}
