/*
 *     Created by Dionis Beqiraj on 10/08/2022, 16:42
 *     dbeqiraj.sohoal@gmail.com
 *     Last modified 10/08/2022, 16:41
 *     Copyright (c) 2022.
 *     All rights reserved.
 */

package com.dbeqiraj.prodapt.ui.posts.navigation.nav

import com.dbeqiraj.prodapt.ui.posts.navigation.nav.impl.PostsNavScreen

object PostsNav {
    val MainNav = PostsNavScreen
}
