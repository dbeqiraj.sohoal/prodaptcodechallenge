/*
 *     Created by Dionis Beqiraj on 10/08/2022, 16:42
 *     dbeqiraj.sohoal@gmail.com
 *     Last modified 10/08/2022, 16:41
 *     Copyright (c) 2022.
 *     All rights reserved.
 */

package com.dbeqiraj.prodapt.ui.posts.navigation.graph.impl

import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavGraphBuilder
import androidx.navigation.NavType
import androidx.navigation.compose.composable
import androidx.navigation.navArgument
import androidx.paging.ExperimentalPagingApi
import com.dbeqiraj.prodapt.ui.common.navigation.NavActions
import com.dbeqiraj.prodapt.ui.posts.navigation.nav.PostsNav
import com.dbeqiraj.prodapt.ui.posts.navigation.nav.impl.PostsNavScreen
import com.dbeqiraj.prodapt.ui.posts.ui.events.PostsListEvents
import com.dbeqiraj.prodapt.ui.posts.ui.screens.PostsListScreen
import com.dbeqiraj.prodapt.ui.posts.ui.screens.details.PostDetailsScreen
import com.dbeqiraj.prodapt.ui.posts.ui.viewmodels.PostsViewModel
import com.google.accompanist.pager.ExperimentalPagerApi
import kotlinx.coroutines.ExperimentalCoroutinesApi

@OptIn(ExperimentalCoroutinesApi::class)
@ExperimentalPagingApi
@ExperimentalPagerApi
@ExperimentalComposeUiApi
fun NavGraphBuilder.postsScreenGraph(navActions: NavActions) {
    // Main Home - list of posts
    composable(PostsNavScreen.PostsListScreen.route) {
        val viewModel: PostsViewModel = hiltViewModel()
        PostsListScreen(viewModel = viewModel) { event ->
            when (event) {
                is PostsListEvents.OnSettings -> {
                    navActions.navigateToSettings()
                }
                is PostsListEvents.OnPostClick -> {
                    navActions.navigateToPostDetails(postId = event.postId.toString())
                    // Log event
                    viewModel.logPostDetailsEvent(postId = event.postId)
                }
                is PostsListEvents.OnNewPage -> {
                    viewModel.logPaginateEvent(postId = event.postId)
                }
                is PostsListEvents.OnScrollFinished -> {
                    viewModel.logPostsScrollEvent(postId = event.lastVisiblePostId)
                }
                else -> {}
            }
        }
    }

    // Post details
    composable(
        route = PostsNav.MainNav.PostDetailsScreen.route,
        arguments = listOf(
            navArgument(PostsNav.MainNav.PostDetailsScreen.argument0) {
                type = NavType.IntType; nullable = false
            }
        )
    ) { backStackEntry ->
        val viewModel: PostsViewModel = hiltViewModel()
        val postId = backStackEntry.arguments?.getInt(PostsNav.MainNav.PostDetailsScreen.argument0)
        PostDetailsScreen(postId = postId, viewModel = viewModel) { event ->
            when (event) {
                is PostsListEvents.OnOpenWebView -> {
                    navActions.navigateWebView(url = event.url)
                }
                else -> {}
            }
        }
    }
}
