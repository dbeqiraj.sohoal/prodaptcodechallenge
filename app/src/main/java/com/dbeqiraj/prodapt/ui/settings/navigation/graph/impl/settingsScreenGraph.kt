package com.dbeqiraj.prodapt.ui.settings.navigation.graph.impl

import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavGraphBuilder
import androidx.navigation.compose.composable
import androidx.paging.ExperimentalPagingApi
import com.dbeqiraj.prodapt.ui.common.navigation.NavActions
import com.dbeqiraj.prodapt.ui.settings.navigation.nav.impl.SettingsNavScreen
import com.dbeqiraj.prodapt.ui.settings.ui.screens.SettingsScreen
import com.dbeqiraj.prodapt.ui.settings.ui.viewmodels.SettingsViewModel
import com.dbeqiraj.prodapt.ui.webview.ui.events.SettingsEvents
import com.google.accompanist.pager.ExperimentalPagerApi

@ExperimentalPagingApi
@ExperimentalPagerApi
@ExperimentalComposeUiApi
fun NavGraphBuilder.settingsScreenGraph(navActions: NavActions) {
    composable(SettingsNavScreen.SettingsScreen.route) {
        val viewModel: SettingsViewModel = hiltViewModel()
        SettingsScreen(viewModel = viewModel) { event ->
            when (event) {
                is SettingsEvents.SaveEmail -> {
                    viewModel.saveEmail(event.email)
                }
                SettingsEvents.NavigateBack -> {
                    navActions.navigateToUp()
                }
            }
        }
    }
}
