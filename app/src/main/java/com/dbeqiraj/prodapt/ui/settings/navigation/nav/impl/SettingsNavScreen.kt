/*
 *     Created by Dionis Beqiraj on 10/08/2022, 16:42
 *     dbeqiraj.sohoal@gmail.com
 *     Last modified 10/08/2022, 16:41
 *     Copyright (c) 2022.
 *     All rights reserved.
 */

package com.dbeqiraj.prodapt.ui.settings.navigation.nav.impl

import com.dbeqiraj.prodapt.routing.NavigationRoute

object SettingsNavScreen {
    val SettingsScreen = object : NavigationRoute {
        override val route: String = "SettingsScreen"
    }
}
