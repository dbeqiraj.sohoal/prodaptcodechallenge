package com.dbeqiraj.prodapt.ui.webview.navigation.graph.impl

import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.navigation.NavGraphBuilder
import androidx.navigation.NavType
import androidx.navigation.compose.composable
import androidx.navigation.navArgument
import androidx.paging.ExperimentalPagingApi
import com.dbeqiraj.prodapt.ui.webview.navigation.nav.WebViewNav
import com.dbeqiraj.prodapt.ui.webview.ui.screens.WebViewScreen
import com.google.accompanist.pager.ExperimentalPagerApi
import kotlinx.coroutines.ExperimentalCoroutinesApi

@OptIn(ExperimentalCoroutinesApi::class)
@ExperimentalPagingApi
@ExperimentalPagerApi
@ExperimentalComposeUiApi
fun NavGraphBuilder.webViewScreenGraph() {
    composable(
        route = WebViewNav.MainNav.WebViewScreen.route,
        arguments = listOf(
            navArgument(WebViewNav.MainNav.WebViewScreen.argument0) {
                type = NavType.StringType; nullable = false
            }
        )
    ) { backStackEntry ->
        val url = backStackEntry.arguments?.getString(WebViewNav.MainNav.WebViewScreen.argument0)
        url?.let { WebViewScreen(url = it) }
    }
}
