/*
 *     Created by Dionis Beqiraj on 10/08/2022, 16:42
 *     dbeqiraj.sohoal@gmail.com
 *     Last modified 10/08/2022, 16:41
 *     Copyright (c) 2022.
 *     All rights reserved.
 */

package com.dbeqiraj.prodapt.ui.posts.navigation.nav.impl

import com.dbeqiraj.prodapt.routing.NavigationRoute
import com.dbeqiraj.prodapt.routing.NavigationRouteArgument1

object PostsNavScreen {
    val PostsListScreen = object : NavigationRoute {
        override val route: String = "PostsListScreen"
    }
    val PostDetailsScreen = object : NavigationRouteArgument1 {
        override val argument0: String = "postId"
        override val route: String = "PostDetailsScreen/{postId}"
    }
}
