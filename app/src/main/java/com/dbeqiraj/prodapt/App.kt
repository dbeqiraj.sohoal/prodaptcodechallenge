/*
 *     Created by Dionis Beqiraj on 10/08/2022, 16:42
 *     dbeqiraj.sohoal@gmail.com
 *     Last modified 10/08/2022, 16:41
 *     Copyright (c) 2022.
 *     All rights reserved.
 */

package com.dbeqiraj.prodapt

import android.app.Application
import androidx.work.OneTimeWorkRequest
import androidx.work.WorkManager
import com.dbeqiraj.prodapt.workers.EmailWorker
import com.dbeqiraj.prodaptkit.applifecyclehandler.AppLifecycleHandler
import com.dbeqiraj.prodaptkit.applifecyclehandler.LifecycleDelegate
import com.dbeqiraj.prodaptkit.initializers.base.impl.AppInitializersSuit
import dagger.hilt.android.HiltAndroidApp
import timber.log.Timber
import javax.inject.Inject

@HiltAndroidApp
class App : Application(), LifecycleDelegate {

    @Inject
    lateinit var initializers: AppInitializersSuit

    override fun onCreate() {
        super.onCreate()
        //
        initializers.init(this)
        // Lifecycle observer
        val lifeCycleHandler = AppLifecycleHandler(this)
        registerLifecycleHandler(lifeCycleHandler)
    }

    override fun onAppBackgrounded() {
        Timber.d("APP::: App is going in background")
        // Start EmailWorker
        val emailWorkerRequest = OneTimeWorkRequest.Builder(EmailWorker::class.java).build()
        WorkManager.getInstance(this).enqueue(emailWorkerRequest)
    }

    override fun onAppForegrounded() {
        Timber.d("APP::: App came in foreground")
    }

    //region - Private funs

    private fun registerLifecycleHandler(lifeCycleHandler: AppLifecycleHandler) {
        registerActivityLifecycleCallbacks(lifeCycleHandler)
        registerComponentCallbacks(lifeCycleHandler)
    }

    //endregion
}
