/*
 *     Created by Dionis Beqiraj on 10/08/2022, 16:42
 *     dbeqiraj.sohoal@gmail.com
 *     Last modified 10/08/2022, 16:41
 *     Copyright (c) 2022.
 *     All rights reserved.
 */

package com.dbeqiraj.prodapt

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.navigation.NavHostController
import androidx.navigation.compose.rememberNavController
import androidx.paging.ExperimentalPagingApi
import com.dbeqiraj.prodapt.base.LocalBaseViewModel
import com.dbeqiraj.prodapt.theme.MaterialThemeCustom
import com.dbeqiraj.prodapt.theme.MyTheme
import com.dbeqiraj.prodapt.ui.common.navigation.NavGraph
import com.dbeqiraj.prodapt.ui.common.ui.screens.PermissionNotGranted
import com.dbeqiraj.prodapt.ui.common.ui.viewmodels.MainViewModel
import com.dbeqiraj.prodaptkit.location.LocationDelegate
import com.google.accompanist.pager.ExperimentalPagerApi
import com.google.accompanist.permissions.ExperimentalPermissionsApi
import com.google.accompanist.permissions.rememberMultiplePermissionsState
import com.google.accompanist.systemuicontroller.rememberSystemUiController
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi
import javax.inject.Inject
import kotlin.time.ExperimentalTime

@OptIn(ExperimentalPermissionsApi::class)
@ExperimentalPagerApi
@ExperimentalComposeUiApi
@ExperimentalPagingApi
@ExperimentalCoroutinesApi
@ExperimentalTime
@AndroidEntryPoint
class MainActivity : ComponentActivity() {

    private val viewModel: MainViewModel by viewModels()

    private lateinit var navController: NavHostController

    @Inject
    internal lateinit var locationDelegate: LocationDelegate

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContent {
            navController = rememberNavController()

            val systemUiController = rememberSystemUiController()
            val useDarkIcons = MaterialThemeCustom.colors.isLight

            CompositionLocalProvider(
                LocalBaseViewModel provides viewModel
            ) {
                MyTheme {
                    // change status bar color
                    systemUiController.setSystemBarsColor(
                        color = MaterialTheme.colors.primaryVariant,
                        darkIcons = useDarkIcons
                    )

                    // Location permission state
                    val locationPermissionState =
                        rememberMultiplePermissionsState(viewModel.locationPermissions)

                    when (locationPermissionState.allPermissionsGranted) {
                        true -> {
                            // Start nav graph
                            NavGraph(navController)
                        }
                        else -> {
                            // Permissions not granted
                            PermissionNotGranted(locationPermissionState = locationPermissionState)
                        }
                    }
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        // Request location update
        locationDelegate.requestLocationUpdate(this)
    }
}
