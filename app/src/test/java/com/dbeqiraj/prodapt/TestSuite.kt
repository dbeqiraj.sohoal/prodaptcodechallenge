package com.dbeqiraj.prodapt

import com.dbeqiraj.prodapt.ui.common.MainViewModelTest
import org.junit.runner.RunWith
import org.junit.runners.Suite

/**
 * Most of our business classes such as repositories and PostViewModel depend on Room db and Datastore which require app context.
 * Therefore they will be exicuted with [AndroidJUnit4] test runner.
 * The MainViewModel test is very simple and it's mainly for demo purposes.
 */
@RunWith(Suite::class)
@Suite.SuiteClasses(
    MainViewModelTest::class
)
class TestSuite
