package com.dbeqiraj.prodapt.base

import org.junit.Assert

object BaseTest {

    infix fun Any?.isEqualTo(value: Any?) = Assert.assertEquals(value, this)

    infix fun Any?.isNotEqualTo(value: Any?) = Assert.assertNotEquals(value, this)

    fun Any?.isNotNull(value: Any?) = Assert.assertNotNull(value)

    fun Any?.isNull(value: Any?) = Assert.assertNull(value)

    infix fun Any?.isTrue(value: Boolean) = assert(value)
}
