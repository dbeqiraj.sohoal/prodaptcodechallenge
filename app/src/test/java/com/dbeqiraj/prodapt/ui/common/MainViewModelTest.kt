package com.dbeqiraj.prodapt.ui.common

import com.dbeqiraj.prodapt.base.BaseTest.isEqualTo
import com.dbeqiraj.prodapt.base.BaseTest.isNotEqualTo
import com.dbeqiraj.prodapt.ui.common.ui.viewmodels.MainViewModel
import com.dbeqiraj.prodapt.ui.posts.navigation.nav.PostsNav
import com.dbeqiraj.prodapt.ui.settings.navigation.nav.SettingsNav
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@OptIn(ExperimentalCoroutinesApi::class)
@RunWith(JUnit4::class)
class MainViewModelTest {

    private val mainViewModel = MainViewModel()

    @Test
    fun `should match the current route in the nav graph`() {
        runTest {
            // Arrange
            mainViewModel.setCurrentRoute(PostsNav.MainNav.PostsListScreen.route)
            // Assert
            mainViewModel.route.value.isEqualTo("PostsListScreen")
        }
    }

    @Test
    fun `should not match the current route in the nav graph`() {
        runTest {
            // Arrange
            mainViewModel.setCurrentRoute(SettingsNav.MainNav.SettingsScreen.route)
            // Assert
            mainViewModel.route.value.isNotEqualTo("PostsListScreen")
        }
    }
}
