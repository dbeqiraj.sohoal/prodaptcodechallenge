import Build_gradle.and
import Build_gradle.dep
import org.jetbrains.kotlin.konan.properties.Properties

typealias and = com.dbeqiraj.internal.Android
typealias dep = com.dbeqiraj.internal.Dependencies

val ktlint by configurations.creating

plugins {
    id("dagger.hilt.android.plugin")
    id("com.android.application")
    id("kotlin-android")
    id("internal")

    kotlin("kapt")
    kotlin("plugin.serialization")
}

val (googleMapsApiKey, emailServiceUsername, emailServicePassword) = File("secrets.properties").run {
    val properties = Properties()
    properties.load(inputStream())
    arrayOf(
        properties.getProperty("google_maps_api_key"),
        properties.getProperty("email_service_username"),
        properties.getProperty("email_service_password")
    )
}

android {

    compileSdk = and.compileSdk

    defaultConfig {
        applicationId = "com.dbeqiraj.prodapt"
        minSdk = and.minSdk
        targetSdk = and.targetSdk
        versionCode = 1
        versionName = "1.0"

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        vectorDrawables {
            useSupportLibrary = true
        }

        // Google Maps Api Key
        manifestPlaceholders["googleMapsApiKey"] = googleMapsApiKey

        // Email secrets
        buildConfigField(
            type = "String",
            name = "EMAIL_SERVICE_USERNAME",
            value = emailServiceUsername
        )
        buildConfigField(
            type = "String",
            name = "EMAIL_SERVICE_PASSWORD",
            value = emailServicePassword
        )
    }
    buildTypes {
        release {
            isMinifyEnabled = true
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }
    buildFeatures {
        compose = true
    }
    composeOptions {
        kotlinCompilerExtensionVersion = com.dbeqiraj.internal.Versions.compose
    }
    packagingOptions {
        resources {
            excludes.add("META-INF/AL2.0")
            excludes.add("META-INF/LGPL2.1")
        }
    }
}

dependencies {

    dep.compose.apply { // https://developer.android.com/jetpack/compose
        implementation(ui)
        implementation(material)
        implementation(tooling)
        implementation(constraint)
        implementation(layout)
        implementation(pagingCompose)
        implementation(icons)
        implementation(hiltNavigationCompose)
        implementation(googleMapsCompose)
    }

    dep.accompanist.apply { // https://google.github.io/accompanist/
        implementation(insets)
        implementation(swipeRefresh)
        implementation(systemuicontroller)
        implementation(pager)
        implementation(permissions)
    }

    dep.playServices.apply { // https://developer.android.com/jetpack/compose
        implementation(playServicesMaps)
        implementation(playServicesLocation)
    }

    dep.datastore.apply { // https://developer.android.com/topic/libraries/architecture/datastore
        implementation(datastore)
    }

    dep.hilt.apply { // https://dagger.dev/hilt/
        implementation(hiltAndroid)
        implementation(hiltWork)
        kapt(hiltCompiler)
        kapt(daggerHiltCompiler)
        kaptAndroidTest(daggerHiltCompiler)
    }

    dep.security.apply { // https://developer.android.com/topic/security/data
        implementation(crypto)
    }

    dep.retrofit.apply { // https://square.github.io/retrofit/
        // Only for testing
        androidTestImplementation(retrofit2)
        androidTestImplementation(interceptor)
        androidTestImplementation(converterMoshi)
    }

    dep.room.apply { // https://developer.android.com/jetpack/androidx/releases/room
        androidTestImplementation(paging)
        androidTestImplementation(runtime)
        androidTestImplementation(ktx)
        kaptAndroidTest(compiler)
    }

    dep.test.apply {
        // unit
        testImplementation(junit)
        testImplementation(mockk)
        testImplementation(coroutinesTest)
        // Android Test
        androidTestImplementation(mockWebServer)
        androidTestImplementation(espresso)
        androidTestImplementation(androidMockk)
        androidTestImplementation(uiTestJunit4)
        debugImplementation(uiTestManifest)
    }

    dep.other.apply { // Miscellaneous required libraries
        implementation(kotlinxCoroutines)
        implementation(kotlinReflect)
        implementation(material)
        implementation(serialization)
        implementation(customTabs)
        implementation(kotlinxDatetime)
        implementation(ktxCore)
        implementation(paging)
        implementation(coil)
        implementation(workManager)
        // For tests only
        androidTestImplementation(gson)
    }

    dep.libraries.apply { // Internal libraries
        implementation(project(prodaptKit))
        implementation(project(data))
        implementation(project(datastore))
        implementation(project(domain))
    }

    dep.checkStyle.apply { // https://ktlint.github.io/
        ktlint(pinterestKtlint) {
            attributes {
                attribute(Bundling.BUNDLING_ATTRIBUTE, objects.named(Bundling.EXTERNAL))
            }
        }
    }

}

// region - Checkstyle

val outputDir = "${project.buildDir}/reports/ktlint/"
val inputFiles = project.fileTree(mapOf("dir" to "src", "include" to "**/*.kt"))

val ktlintCheck by tasks.creating(JavaExec::class) {
    inputs.files(inputFiles)
    outputs.dir(outputDir)

    description = "Check Kotlin code style."
    classpath = ktlint
    mainClass.set("com.pinterest.ktlint.Main")
    args = listOf("src/**/*.kt")
}

val ktlintFormat by tasks.creating(JavaExec::class) {
    inputs.files(inputFiles)
    outputs.dir(outputDir)

    description = "Fix Kotlin code style deviations."
    classpath = ktlint
    mainClass.set("com.pinterest.ktlint.Main")
    args = listOf("-F", "src/**/*.kt")
}

// endregion