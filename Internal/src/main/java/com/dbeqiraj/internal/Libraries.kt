/*
 *     Created by Dionis Beqiraj on 10/08/2022, 16:42
 *     dbeqiraj.sohoal@gmail.com
 *     Last modified 10/08/2022, 16:41
 *     Copyright (c) 2022.
 *     All rights reserved.
 */

package com.dbeqiraj.internal

object Libraries {
    /**
     * Internal library [data]
     */
    const val data = ":data"

    /**
     * Internal library [datastore]
     */
    const val datastore = ":datastore"

    /**
     * Internal library [domain]
     */
    const val domain = ":domain"

    /**
     * Internal library [ProdaptKit]
     */
    const val prodaptKit = ":ProdaptKit"

    /**
     * Internal library [annotations]
     */
    const val annotations = ":annotations"

    /**
     * Internal library [annotationsProcessor]
     */
    const val annotationsProcessor = ":annotations-processor"
}
