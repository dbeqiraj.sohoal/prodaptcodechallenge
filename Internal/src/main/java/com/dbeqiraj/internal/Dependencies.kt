/*
 *     Created by Dionis Beqiraj on 10/08/2022, 16:42
 *     dbeqiraj.sohoal@gmail.com
 *     Last modified 10/08/2022, 16:41
 *     Copyright (c) 2022.
 *     All rights reserved.
 */

@file:Suppress("unused")

package com.dbeqiraj.internal

import com.dbeqiraj.internal.dependencies.*

object Dependencies {
    val accompanist = Accompanist
    val android = Android
    val annotations = Annotations
    val compose = Compose
    val datastore = Datastore
    val hilt = Di
    val libraries = Libraries
    val other = Other
    val retrofit = Retrofit
    val room = Room
    val security = Security
    val test = Test
    val playServices = PlayServices
    val checkStyle = CheckStyle
}
