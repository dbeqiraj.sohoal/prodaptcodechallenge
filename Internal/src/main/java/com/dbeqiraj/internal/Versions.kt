/*
 *     Created by Dionis Beqiraj on 10/08/2022, 16:42
 *     dbeqiraj.sohoal@gmail.com
 *     Last modified 10/08/2022, 16:41
 *     Copyright (c) 2022.
 *     All rights reserved.
 */

@file:Suppress("unused")

package com.dbeqiraj.internal

object Versions {
    // jetpack
    const val ktxCore = "1.5.0"

    // compose
    const val compose = "1.1.0"
    const val constraint = "1.0.0"
    const val pagingCompose = "1.0.0-alpha14"
    const val paging = "3.0.0"
    const val accompanist = "0.15.0"

    // Maps
    const val googleMaps = "2.5.3"
    const val playServicesMaps = "18.1.0"
    const val playServicesLocation = "20.0.0"

    // android
    const val material = "1.4.0-rc01"

    // annotations
    const val autoService = "1.0.1"
    const val stdLib = "1.6.10"

    // datastore
    const val datastoreCore = "1.0.0"
    const val protobuf = "3.18.0"

    // di
    const val hilt = "1.0.0"
    const val hiltCore = "2.41"
    const val hiltComposeNavigation = "1.0.0"

    // security
    const val securityCrypto = "1.1.0-alpha03"
    const val securityIdentityCredential = "1.0.0-alpha02"

    // logging
    const val timber = "4.7.1"

    // architecture components
    const val startup = "1.0.0"

    // check style
    const val checkStyle = "0.47.0"

    // test
    const val mockWebServer = "4.10.0"
    const val mockk = "1.12.5"
    const val coroutineTest = "1.6.4"
    const val espresso = "3.4.0"

    // room
    const val room = "2.4.2"
    const val roomPaging = "2.5.0-alpha01"

    // retrofit2
    const val retrofit2 = "2.9.0"
    const val interceptor = "4.9.1"

    // other
    const val dokka = "1.4.32"
    const val sandwich = "1.1.0"
    const val glide = "0.10.0"
    const val serialization = "1.2.1"
    const val gson = "2.9.1"
    const val customTabs = "1.3.0"
    const val kotlinxDatetime = "0.2.1"
    const val kotlinxCoroutines = "1.6.4"
    const val kotlinReflect = "1.6.10"
    const val moshiLazyAdapter = "2.2"
    const val coil = "2.1.0"
    const val sunMail = "1.6.4"
    const val workManager = "2.7.1"
}
