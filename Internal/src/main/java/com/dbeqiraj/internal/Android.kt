/*
 *     Created by Dionis Beqiraj on 10/08/2022, 16:42
 *     dbeqiraj.sohoal@gmail.com
 *     Last modified 10/08/2022, 16:41
 *     Copyright (c) 2022.
 *     All rights reserved.
 */

@file:Suppress("unused")

package com.dbeqiraj.internal

object Android {
    const val minSdk = 21
    const val targetSdk = 31
    const val compileSdk = 31
}
