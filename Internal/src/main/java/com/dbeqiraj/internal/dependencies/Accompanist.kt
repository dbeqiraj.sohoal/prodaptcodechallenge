/*
 *     Created by Dionis Beqiraj on 10/08/2022, 16:42
 *     dbeqiraj.sohoal@gmail.com
 *     Last modified 10/08/2022, 16:41
 *     Copyright (c) 2022.
 *     All rights reserved.
 */

@file:Suppress("unused")

package com.dbeqiraj.internal.dependencies

import com.dbeqiraj.internal.Versions

object Accompanist {
    /**
     * [Accompanist Insets](https://github.com/google/accompanist)
     */
    const val insets = "com.google.accompanist:accompanist-insets:${Versions.accompanist}"

    /**
     * [Swipe Refresh](https://google.github.io/accompanist/swiperefresh/)
     */
    const val swipeRefresh = "com.google.accompanist:accompanist-swiperefresh:${Versions.accompanist}"

    /**
     * [Ui Controller](https://google.github.io/accompanist/systemuicontroller/)
     */
    const val systemuicontroller = "com.google.accompanist:accompanist-systemuicontroller:${Versions.accompanist}"

    /**
     * [Pager](https://google.github.io/accompanist/pager/)
     */
    const val pager = "com.google.accompanist:accompanist-pager:${Versions.accompanist}"

    /**
     * [Permissions](https://google.github.io/accompanist/pager/)
     */
    const val permissions = "com.google.accompanist:accompanist-permissions:${Versions.accompanist}"
}
