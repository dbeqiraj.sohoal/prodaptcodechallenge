/*
 *     Created by Dionis Beqiraj on 10/08/2022, 16:42
 *     dbeqiraj.sohoal@gmail.com
 *     Last modified 10/08/2022, 16:41
 *     Copyright (c) 2022.
 *     All rights reserved.
 */

@file:Suppress("unused")

package com.dbeqiraj.internal.dependencies

import com.dbeqiraj.internal.Versions

object Test {
    /**
     * [JUnit](https://mvnrepository.com/artifact/junit/junit)
     * JUnit is a unit testing framework for Java, created by Erich Gamma and Kent Beck.
     */
    const val junit = "junit:junit:4.+"

    /**
     * [MockWebServer](https://github.com/square/okhttp/tree/master/mockwebserver)
     * A scriptable web server for testing HTTP clients
     */
    const val mockWebServer = "com.squareup.okhttp3:mockwebserver:${Versions.mockWebServer}"

    /**
     * [Mockk](https://mockk.io/)
     * Most popular mocking framework for Kotlin
     */
    const val mockk = "io.mockk:mockk:${Versions.mockk}"

    /**
     * [Android Mockk](https://mockk.io/)
     * Most popular mocking framework for Kotlin
     */
    const val androidMockk = "io.mockk:mockk-android:${Versions.mockk}"

    /**
     * [UI Test Junit4](https://developer.android.com/jetpack/compose/testing#setup)
     */
    const val uiTestJunit4 = "androidx.compose.ui:ui-test-junit4:${Versions.compose}"

    /**
     * [UI Test Manifest](https://developer.android.com/jetpack/compose/testing#setup)
     */
    const val uiTestManifest = "androidx.compose.ui:ui-test-manifest:${Versions.compose}"

    /**
     * [Coroutine Test](https://kotlinlang.org/api/kotlinx.coroutines/kotlinx-coroutines-test/)
     */
    const val coroutinesTest = "org.jetbrains.kotlinx:kotlinx-coroutines-test:${Versions.coroutineTest}"

    /**
     * [Espresso](https://mvnrepository.com/artifact/androidx.test.espresso/espresso-core)
     */
    const val espresso = "androidx.test.espresso:espresso-core:${Versions.espresso}"
}
