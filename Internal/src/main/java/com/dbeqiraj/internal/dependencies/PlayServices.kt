@file:Suppress("unused")

package com.dbeqiraj.internal.dependencies

import com.dbeqiraj.internal.Versions

object PlayServices {

    /**
     * [Play Services Maps](https://mvnrepository.com/artifact/com.google.android.gms/play-services-maps)
     */
    const val playServicesMaps = "com.google.android.gms:play-services-maps:${Versions.playServicesMaps}"

    /**
     * [Play Services Location](https://mvnrepository.com/artifact/com.google.android.gms/play-services-location)
     */
    const val playServicesLocation = "com.google.android.gms:play-services-location:${Versions.playServicesLocation}"
}
