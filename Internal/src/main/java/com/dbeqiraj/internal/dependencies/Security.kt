/*
 *     Created by Dionis Beqiraj on 10/08/2022, 16:42
 *     dbeqiraj.sohoal@gmail.com
 *     Last modified 10/08/2022, 16:41
 *     Copyright (c) 2022.
 *     All rights reserved.
 */

@file:Suppress("unused")

package com.dbeqiraj.internal.dependencies

import com.dbeqiraj.internal.Versions

object Security {
    /**
     * [Crypto](https://developer.android.com/jetpack/androidx/releases/security)
     */
    const val crypto = "androidx.security:security-crypto:${Versions.securityCrypto}"
}
