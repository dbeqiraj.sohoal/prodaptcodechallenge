/*
 *     Created by Dionis Beqiraj on 10/08/2022, 16:42
 *     dbeqiraj.sohoal@gmail.com
 *     Last modified 10/08/2022, 16:41
 *     Copyright (c) 2022.
 *     All rights reserved.
 */

@file:Suppress("unused")

package com.dbeqiraj.internal.dependencies

import com.dbeqiraj.internal.Versions

object Other {
    /**
     * [Material Components For Android](https://mvnrepository.com/artifact/com.google.android.material/material)
     * Material Components for Android is a static library that you can add to your Android application in order to use APIs that provide
     * implementations of the Material Design specification. Compatible on devices running API 14 or later.
     */
    const val material = "com.google.android.material:material:${Versions.material}"

    /**
     * [Timber](https://mvnrepository.com/artifact/com.jakewharton.timber/timber)
     * No-nonsense injectable logging.
     */
    const val timber = "com.jakewharton.timber:timber:${Versions.timber}"

    /**
     * [Android App Startup Runtime](https://mvnrepository.com/artifact/androidx.startup/startup-runtime)
     */
    const val startup = "androidx.startup:startup-runtime:${Versions.startup}"

    /**
     * [Dokka](https://github.com/Kotlin/dokka)
     * Dokka is a documentation engine for Kotlin, performing the same function as javadoc for Java. Just like Kotlin itself, Dokka fully
     * supports mixed-language Java/Kotlin projects. It understands standard Javadoc comments in Java files and KDoc comments in Kotlin files,
     * and can generate documentation in multiple formats including standard Javadoc, HTML and Markdown.
     */
    const val dokka = "org.jetbrains.dokka:kotlin-as-java-plugin:${Versions.dokka}"

    /**
     * [Kotlin multiplatform serialization](https://github.com/Kotlin/kotlinx.serialization)
     */
    const val serialization = "org.jetbrains.kotlinx:kotlinx-serialization-json:${Versions.serialization}"

    /**
     * [Gson](https://github.com/google/gson)
     */
    const val gson = "com.google.code.gson:gson:${Versions.gson}"

    /**
     * [Custom Tabs]( https://developer.chrome.com/docs/android/custom-tabs/overview/)
     */
    const val customTabs = "androidx.browser:browser:${Versions.customTabs}"

    /**
     * [Kotlinx DateTime](https://github.com/Kotlin/kotlinx-datetime)
     */
    const val kotlinxDatetime = "org.jetbrains.kotlinx:kotlinx-datetime:${Versions.kotlinxDatetime}"

    /**
     * [Paging](https://developer.android.com/jetpack/androidx/releases/paging)
     */
    const val paging = "androidx.paging:paging-runtime:${Versions.paging}"

    /**
     * [Core Kotlin Extensions](https://developer.android.com/kotlin/ktx#core)
     * Kotlin extensions for 'core' artifact
     */
    const val ktxCore = "androidx.core:core-ktx:${Versions.ktxCore}"

    /**
     * [Kotlinx Coroutines](https://github.com/Kotlin/kotlinx.coroutines)
     */
    const val kotlinxCoroutines = "org.jetbrains.kotlinx:kotlinx-coroutines-core:${Versions.kotlinxCoroutines}"

    /**
     * [Kotlin Reflect](https://mvnrepository.com/artifact/org.jetbrains.kotlin/kotlin-reflect)
     */
    const val kotlinReflect = "org.jetbrains.kotlin:kotlin-reflect:${Versions.kotlinReflect}"

    /**
     * [Moshi Lazy Adapters](https://github.com/serj-lotutovici/moshi-lazy-adapters)
     */
    const val moshiLazyAdapter = "com.serjltt.moshi:moshi-lazy-adapters:${Versions.moshiLazyAdapter}"

    /**
     * [Coil - Async Image](https://coil-kt.github.io/coil/compose/)
     */
    const val coil = "io.coil-kt:coil-compose:${Versions.coil}"

    /**
     * [Android Mail](https://frontbackend.com/maven/artifact/com.sun.mail/android-mail/1.6.4)
     */
    const val sunMail = "com.sun.mail:android-mail:${Versions.sunMail}"

    /**
     * [Android WorkManager](https://developer.android.com/jetpack/androidx/releases/work)
     */
    const val workManager = "androidx.work:work-runtime-ktx:${Versions.workManager}"
}
