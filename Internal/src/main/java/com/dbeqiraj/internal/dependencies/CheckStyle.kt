package com.dbeqiraj.internal.dependencies

import com.dbeqiraj.internal.Versions

object CheckStyle {
    /**
     * [Ktlint](https://ktlint.github.io/)
     */
    const val pinterestKtlint = "com.pinterest:ktlint:${Versions.checkStyle}"
}