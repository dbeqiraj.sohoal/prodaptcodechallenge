/*
 *     Created by Dionis Beqiraj on 10/08/2022, 16:42
 *     dbeqiraj.sohoal@gmail.com
 *     Last modified 10/08/2022, 16:41
 *     Copyright (c) 2022.
 *     All rights reserved.
 */

@file:Suppress("unused")

package com.dbeqiraj.internal.dependencies

import com.dbeqiraj.internal.Versions

object Retrofit {
    /**
     * [Retrofit](https://mvnrepository.com/artifact/com.squareup.retrofit2/retrofit)
     * A type-safe HTTP client for Android and Java.
     */
    const val retrofit2 = "com.squareup.retrofit2:retrofit:${Versions.retrofit2}"

    /**
     * [OkHttp Logging Interceptor](https://mvnrepository.com/artifact/com.squareup.okhttp3/logging-interceptor)
     * Square’s meticulous HTTP client for Java and Kotlin.
     */
    const val interceptor = "com.squareup.okhttp3:logging-interceptor:${Versions.interceptor}"

    /**
     * [Moshi Converter](https://search.maven.org/artifact/com.squareup.retrofit2/converter-moshi)
     * A Converter which uses Moshi for serialization to and from JSON.
     */
    const val converterMoshi = "com.squareup.retrofit2:converter-moshi:${Versions.retrofit2}"
}
