/*
 *     Created by Dionis Beqiraj on 10/08/2022, 16:42
 *     dbeqiraj.sohoal@gmail.com
 *     Last modified 10/08/2022, 16:41
 *     Copyright (c) 2022.
 *     All rights reserved.
 */

@file:Suppress("unused")

package com.dbeqiraj.internal.dependencies

import com.dbeqiraj.internal.Versions

object Di {
    /**
     * [Hilt Compiler](https://developer.android.com/training/dependency-injection/hilt-jetpack#workmanager)
     */
    const val hiltCompiler = "androidx.hilt:hilt-compiler:${Versions.hilt}"

    /**
     * [Hilt Android](https://mvnrepository.com/artifact/com.google.dagger/hilt-android)
     * A fast dependency injector for Android and Java.
     */
    const val hiltAndroid = "com.google.dagger:hilt-android:${Versions.hiltCore}"

    /**
     * [Hilt Work](https://mvnrepository.com/artifact/androidx.hilt/hilt-work)
     * A fast dependency injector for Android and Java.
     */
    const val hiltWork = "androidx.hilt:hilt-work:${Versions.hilt}"

    /**
     * [Hilt Processor](https://mvnrepository.com/artifact/com.google.dagger/hilt-compiler)
     * A fast dependency injector for Android and Java.
     */
    const val daggerHiltCompiler = "com.google.dagger:hilt-compiler:${Versions.hiltCore}"
}
