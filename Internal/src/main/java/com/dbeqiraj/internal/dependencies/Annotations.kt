/*
 *     Created by Dionis Beqiraj on 10/08/2022, 16:42
 *     dbeqiraj.sohoal@gmail.com
 *     Last modified 10/08/2022, 16:41
 *     Copyright (c) 2022.
 *     All rights reserved.
 */

package com.dbeqiraj.internal.dependencies

import com.dbeqiraj.internal.Versions

object Annotations {
    /**
     * [Auto Service](https://mvnrepository.com/artifact/com.google.auto.service/auto-service)
     */
    const val autoService = "com.google.auto.service:auto-service:${Versions.autoService}"

    /**
     * [Kotlin Std lib](https://mvnrepository.com/artifact/com.google.auto.service/auto-service)
     */
    const val kotlinStdLib = "org.jetbrains.kotlin:kotlin-stdlib-jdk7:${Versions.stdLib}"
}
