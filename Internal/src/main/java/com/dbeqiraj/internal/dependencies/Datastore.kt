/*
 *     Created by Dionis Beqiraj on 10/08/2022, 16:42
 *     dbeqiraj.sohoal@gmail.com
 *     Last modified 10/08/2022, 16:41
 *     Copyright (c) 2022.
 *     All rights reserved.
 */

package com.dbeqiraj.internal.dependencies

import com.dbeqiraj.internal.Versions

object Datastore {

    /**
     * [Datastore](https://developer.android.com/topic/libraries/architecture/datastore)
     */
    const val datastorePreferences = "androidx.datastore:datastore-preferences:${Versions.datastoreCore}"

    /**
     * [Datastore](https://developer.android.com/topic/libraries/architecture/datastore)
     */
    const val datastore = "androidx.datastore:datastore:${Versions.datastoreCore}"

    /**
     * [Datastore](https://developer.android.com/topic/libraries/architecture/datastore)
     */
    const val datastoreCore = "androidx.datastore:datastore-core:${Versions.datastoreCore}"
    const val datastoreProtoBuf = "com.google.protobuf:protobuf-javalite:${Versions.protobuf}"
}
